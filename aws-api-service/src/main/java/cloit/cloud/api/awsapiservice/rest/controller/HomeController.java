package cloit.cloud.api.awsapiservice.rest.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/aws")
public class HomeController {

    @RequestMapping("/home")
    public Mono<String> home(){
        return Mono.just("##### AWS API HOME");
    }
}
