package cloit.cloud.api.core.component.http.client;


import cloit.cloud.api.core.component.http.domain.HttpRequest;
import cloit.cloud.api.core.component.http.domain.HttpResponse;

public interface HttpClient {
    HttpResponse execute(HttpRequest request);
}
