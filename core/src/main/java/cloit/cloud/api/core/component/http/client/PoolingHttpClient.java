package cloit.cloud.api.core.component.http.client;

import cloit.cloud.api.core.component.http.domain.HttpRequest;
import cloit.cloud.api.core.component.http.domain.HttpResponse;
import cloit.cloud.api.core.component.http.exception.HttpClientException;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.impl.client.CloseableHttpClient;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("poolingHttpClient")
@Slf4j
public class PoolingHttpClient implements HttpClient {
    private final CloseableHttpClient closeableHttpClient;

    public PoolingHttpClient(@Qualifier("poolingCloseableHttpClient") CloseableHttpClient closeableHttpClient) {
        this.closeableHttpClient = closeableHttpClient;
    }

    @Override
    public HttpResponse execute(HttpRequest httpRequest) {
        CloseableHttpResponse closeableHttpResponse = null;
        try {
            log.debug("##### Execute http client: [httpRequest = {}]", httpRequest);

            final HttpUriRequest httpUriRequest = httpRequest.build();
            closeableHttpResponse = closeableHttpClient.execute(httpUriRequest);

            final int statusCode = closeableHttpResponse.getStatusLine().getStatusCode();
            final Header[] headers = closeableHttpResponse.getAllHeaders();
            final HttpEntity entity = closeableHttpResponse.getEntity();

            final HttpResponse httpResponse = new HttpResponse(statusCode, headers, entity);
            log.debug("##### Successful execute http client: [httpResponse = {}]", httpResponse);

            return httpResponse;
        } catch (Exception e) {
            log.debug("##### Failed to execute http client", e);
            throw new HttpClientException("##### Failed to execute http client", e);
        } finally {
            HttpClientUtils.closeQuietly(closeableHttpResponse);
        }
    }
}
