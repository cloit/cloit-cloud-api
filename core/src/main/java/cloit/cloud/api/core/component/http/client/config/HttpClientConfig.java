package cloit.cloud.api.core.component.http.client.config;

import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.concurrent.TimeUnit;

@Configuration
@Slf4j
public class HttpClientConfig {
    private static final int MAX_CONNECTION_SIZE_PER_ROUTE  = 10;
    private static final int MAX_CONNECTION_SIZE            = 100;

    private static final int CONNECTION_TIMEOUT             = 30 * 1000;
    private static final int CONNECTION_REQUEST_TIMEOUT     = 30 * 1000;
    private static final int SOCKET_TIMEOUT                 = 180 * 1000;

    private static final int IDLE_TIMEOUT                   = 30 * 1000;

    @Bean
    public CloseableHttpClient poolingCloseableHttpClient() {
        return HttpClients.custom()
                .setDefaultRequestConfig(requestConfig())
                .setConnectionManager(poolingHttpClientConnectionManager())
                .build();
    }

    @Bean
    public RequestConfig requestConfig() {
        return RequestConfig.custom()
                .setConnectTimeout(CONNECTION_TIMEOUT)
                .setConnectionRequestTimeout(CONNECTION_REQUEST_TIMEOUT)
                .setSocketTimeout(SOCKET_TIMEOUT)
                .build();
    }

    @Bean
    public PoolingHttpClientConnectionManager poolingHttpClientConnectionManager() {
        PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager();
        connectionManager.setDefaultMaxPerRoute(MAX_CONNECTION_SIZE_PER_ROUTE);
        connectionManager.setMaxTotal(MAX_CONNECTION_SIZE);
        return connectionManager;
    }

    @Bean
    public Runnable idleConnectionMonitor(final PoolingHttpClientConnectionManager poolingHttpClientConnectionManager) {
        return new Runnable() {
            @Scheduled(fixedDelay = 30 * 1000)
            @Override
            public void run() {
                try {
                    poolingHttpClientConnectionManager.closeExpiredConnections();
                    poolingHttpClientConnectionManager.closeIdleConnections(IDLE_TIMEOUT, TimeUnit.MILLISECONDS);
                    log.debug("##### Successful closing of expired/idle http connections.");
                } catch (Exception e) {
                    log.debug("##### Failed to close expired/idle http connections.");
                    throw new RuntimeException("##### Failed to close expired/idle http connections.", e);
                }
            }
        };
    }

}
