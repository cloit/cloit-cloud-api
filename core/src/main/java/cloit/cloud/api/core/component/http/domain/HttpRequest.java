package cloit.cloud.api.core.component.http.domain;

import cloit.cloud.api.core.component.http.exception.HttpRequestException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringSubstitutor;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.*;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.core.codec.EncodingException;
import org.springframework.http.HttpMethod;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.apache.http.entity.ContentType.*;
import static org.springframework.http.HttpMethod.*;

@Data
@Slf4j
public class HttpRequest {
    private static final String PATH_PARAM_PREFIX = "{";
    private static final String PATH_PARAM_SUFFIX = "}";
    private static final Gson GSON = new GsonBuilder().create();

    private HttpMethod httpMethod;
    private String host;
    private String path;
    private Map<String, Object> queryParams;
    private ContentType contentType;
    private final Map<String, String> headers;
    private Object entity;
    private final Map<String, Object> entities;

    public HttpRequest() {
        this.queryParams = new LinkedHashMap<>();
        this.headers = new LinkedHashMap<>();
        this.entities = new LinkedHashMap<>();
        this.post();
        this.setContentType(APPLICATION_JSON);
    }

    public static HttpRequest create() {
        return new HttpRequest();
    }

    public HttpRequest get() {
        this.httpMethod = GET;
        return this;
    }

    public HttpRequest post() {
        this.httpMethod = POST;
        return this;
    }

    public HttpRequest put() {
        this.httpMethod = PUT;
        return this;
    }

    public HttpRequest delete() {
        this.httpMethod = HttpMethod.DELETE;
        return this;
    }

    public HttpRequest setHost(String host) {
        this.host = host;
        return this;
    }

    public HttpRequest setPath(String path) {
        this.path = path;
        return this;
    }

    public HttpRequest setPathParam(String key, String value) {
        final Map<String, String> pathParams = new LinkedHashMap<>();
        pathParams.put(key, value);

        this.path = new StringSubstitutor(pathParams, PATH_PARAM_PREFIX, PATH_PARAM_SUFFIX).replace(this.path);
        return this;
    }

    public HttpRequest setQueryParams(String key, Object value){
        this.queryParams.put(key, value);
        return this;
    }

    public HttpRequest setContentType(ContentType contentType) {
        this.contentType = contentType;
        this.setHeader("Content-Type", contentType.getMimeType(), true);
        return this;
    }

    public HttpRequest setHeader(String key, String value) {
        return setHeader(key, value, false);
    }

    public HttpRequest setHeader(String key, String value, boolean force) {
        final boolean isContainsKey = this.headers.containsKey(key);
        if (isContainsKey) {
            if (force) {
                this.headers.put(key, value);
            }
        } else {
            this.headers.put(key, value);
        }
        return this;
    }

    public HttpRequest setEntity(Object value) {
        this.entity = value;
        return this;
    }

    public HttpRequest setEntity(String key, Object value) {
        this.entities.put(key, value);
        return this;
    }

    public HttpMethod getHttpMethod() {
        return httpMethod;
    }

    public String getHost() {
        return host;
    }

    public String getPath() {
        return this.path;
    }

    public URI getURI() {
        final UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder.newInstance();
        for( String key : queryParams.keySet() ) {
            Object obj = queryParams.get(key);
            if( obj == null ){
                continue;
            }
            uriComponentsBuilder.queryParam(key, queryParams.get(key));
        }
        return URI.create(this.host + getPath() + uriComponentsBuilder.toUriString());
    }

    public String getQuery(){
        StringBuilder stringBuilder = new StringBuilder();
        queryParams.forEach((k, v) -> {
            stringBuilder.append("&").append(k).append("=").append(v);
        });
        if( stringBuilder.length() > 0 ){
            stringBuilder.deleteCharAt(0);
            return stringBuilder.toString();
        }else{
            return null;
        }
    }

    public Header[] getHeaders() {
        return this.headers.entrySet()
                .stream()
                .map(entry -> new BasicHeader(entry.getKey(), entry.getValue()))
                .toArray(Header[]::new);
    }

    public Object getEntityAsObject() {
        final boolean isJsonRoot = (this.entity != null);
        if (isJsonRoot) {
            return this.entity;
        } else {
            return this.entities;
        }
    }

    public String getEntityAsJson() {
        final Object entity = getEntityAsObject();
        return GSON.toJson(entity);
    }

    private HttpEntity getEntity() {
        try {
            if (this.contentType.equals(APPLICATION_JSON)) {
                final String entityInJson = getEntityAsJson();
                return new StringEntity(entityInJson, StandardCharsets.UTF_8);
            }

            if (this.contentType.equals(APPLICATION_FORM_URLENCODED)) {
                final List<NameValuePair> entitiesInNameValuePair = this.entities.entrySet()
                        .stream()
                        .map(entry -> new BasicNameValuePair(entry.getKey(), entry.getValue().toString()))
                        .collect(Collectors.toList());
                return new UrlEncodedFormEntity(entitiesInNameValuePair, StandardCharsets.UTF_8);
            }

            if (this.contentType.equals(TEXT_HTML)) {
                final Object entity = this.entity;
                return new StringEntity((String) entity, StandardCharsets.UTF_8);
            }

            return new StringEntity(StringUtils.EMPTY);
        } catch (UnsupportedEncodingException e) {
            throw new EncodingException("##### Unsupported encoding", e);
        }
    }

    public HttpUriRequest build() {
        final URI uri           = getURI();
        final Header[] headers  = getHeaders();
        final HttpEntity entity = getEntity();

        if (this.httpMethod.equals(GET)) {
            final HttpGet httpGet = new HttpGet(uri);
            httpGet.setHeaders(headers);
            return httpGet;
        } else if (this.httpMethod.equals(POST)) {
            final HttpPost httpPost = new HttpPost(uri);
            httpPost.setHeaders(headers);
            httpPost.setEntity(entity);
            return httpPost;
        } else if (this.httpMethod.equals(PUT)) {
            final HttpPut httpPut = new HttpPut(uri);
            httpPut.setHeaders(headers);
            httpPut.setEntity(entity);
            return httpPut;
        } else if (this.httpMethod.equals(DELETE)) {
            final HttpDelete httpDelete = new HttpDelete(uri);
            httpDelete.setHeaders(headers);
            return httpDelete;
        }
        log.debug("##### Not found http method: [httpMethod -> {}]", httpMethod);
        throw new HttpRequestException("##### Not found http method");
    }

    public HttpRequest setQueryParams(Object queryData) {
        ObjectMapper objectMapper = new ObjectMapper();
        this.queryParams = objectMapper.convertValue(queryData, new TypeReference<>(){});
        return this;
    }
}
