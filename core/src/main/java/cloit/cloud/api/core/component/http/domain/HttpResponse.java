package cloit.cloud.api.core.component.http.domain;

import cloit.cloud.api.core.component.http.exception.HttpResponseException;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.Consts;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.util.EntityUtils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

import static org.apache.http.entity.ContentType.*;

@Data
@Slf4j
public class HttpResponse {
    private static final String CONTENT_TYPE = "Content-Type";
    private static final ContentType TEXT_JSON = create("text/json", Consts.UTF_8);

    private final int statusCode;
    private final Header[] headers;
    private final HttpEntity entity;

    private String entityAsJson;
    private InputStream entityAsInputStream;

    public HttpResponse(int statusCode, Header[] headers, HttpEntity entity) {
        this.statusCode = statusCode;
        this.headers = headers;
        this.entity = entity;

        final Header contentType = Arrays.stream(headers)
                .filter(it -> it.getName().equals(CONTENT_TYPE))
                .findFirst()
                .orElseThrow(() -> new HttpResponseException("##### Not found Content-Type in headers"));

        final String mimeType = contentType.getValue();

        if (mimeType.contains(APPLICATION_JSON.getMimeType())
                || mimeType.contains(TEXT_JSON.getMimeType())
                || mimeType.contains(TEXT_HTML.getMimeType())) {
            handleResponseForString();
        } else if (mimeType.contains(APPLICATION_OCTET_STREAM.getMimeType())
                || mimeType.contains(IMAGE_PNG.getMimeType())) {
            handleResponseForFile();
        } else {
            handleResponseForString();
        }
    }

    private void handleResponseForString() {
        try {
            this.entityAsJson = EntityUtils.toString(this.entity, StandardCharsets.UTF_8);
        } catch (IOException e) {
            log.debug("##### Failed to handle response for string", e);
            throw new HttpResponseException("##### Failed to handle response for string", e);
        }
    }

    private void handleResponseForFile() {
        ByteArrayOutputStream byteArrayOutputStream = null;
        try {
            final InputStream inputStream = this.entity.getContent();
            byteArrayOutputStream = new ByteArrayOutputStream();

            final byte[] buffer = new byte[1024];
            int len;
            while ((len = inputStream.read(buffer)) > -1 ) {
                byteArrayOutputStream.write(buffer, 0, len);
            }

            this.entityAsInputStream = new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
        } catch (IOException e) {
            log.debug("##### Failed to handle response for file", e);
            throw new HttpResponseException("##### Failed to handle response for file", e);
        } finally {
            try {
                if (byteArrayOutputStream != null) {
                    byteArrayOutputStream.flush();
                }
            } catch (IOException e) {
                log.debug("##### Failed to handle response for file", e);
            }
        }
    }
}
