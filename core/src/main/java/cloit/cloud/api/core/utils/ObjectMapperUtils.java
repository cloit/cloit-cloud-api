package cloit.cloud.api.core.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;

import java.lang.reflect.Type;
import java.util.Map;

public class ObjectMapperUtils {

    /**
     * Map -> Object
     * @param map map
     * @param type 변환될 객체 타입
     * @return T
     */
    public static <T> T mapToObject(Map<String, Object> map, Type type){
        final ObjectMapper objectMapper = new ObjectMapper();
        TypeFactory typeFactory = objectMapper.getTypeFactory();
        return objectMapper.convertValue(map, typeFactory.constructType(type));
    }
}
