package cloit.cloud.api.ncpapiservice.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication(
        scanBasePackages = {"cloit.cloud.api.ncpapiservice", "cloit.cloud.api.core"}
)
@EnableDiscoveryClient
public class NcpApiServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(NcpApiServiceApplication.class, args);
    }

}
