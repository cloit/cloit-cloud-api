package cloit.cloud.api.ncpapiservice.rest.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/ncp")
@Slf4j
public class HomeController {

    @RequestMapping("/home")
    public Mono<String> home(){
        return Mono.just("##### NCP API HOME");
    }
}
