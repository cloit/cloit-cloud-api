package cloit.cloud.api.ncpapiservice.v1.api.comm.constants;

import cloit.cloud.api.ncpapiservice.v1.api.compute.server.server.domain.request.server.CreateServerInstanceRequest;
import cloit.cloud.api.ncpapiservice.v1.api.compute.server.server.domain.request.server.StartServerInstanceRequest;
import lombok.Getter;

import java.util.Arrays;
import java.util.List;

public enum ConverterClass {

    CREATE_SERVER_INSTANCE_REQUEST(
            "CREATE_SERVER_INSTANCE_REQUEST",
            CreateServerInstanceRequest.class,
            Arrays.asList("networkInterfaceList", "blockDevicePartitionList", "blockStorageMappingList")
    ),
    START_SERVER_INSTANCE_REQUEST(
            "START_SERVER_INSTANCE_REQUEST",
            StartServerInstanceRequest.class,
            List.of("serverInstanceNoList")
    )
    ;

    @Getter
    private final String dataName;

    @Getter
    private final Class<?> clazz;

    @Getter
    private final List<String> converterPropertyNames;

    ConverterClass(String dataName, Class<?> clazz, List<String> converterPropertyNames) {
        this.dataName = dataName;
        this.clazz = clazz;
        this.converterPropertyNames = converterPropertyNames;
    }

    public static ConverterClass findConvertClass(Class<?> clazz){
        return Arrays.stream(values())
                .filter(converterClass -> converterClass.getClazz().equals(clazz))
                .findAny()
                .orElse(null);
    }
}
