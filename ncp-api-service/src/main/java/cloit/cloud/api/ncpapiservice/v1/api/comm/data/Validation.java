package cloit.cloud.api.ncpapiservice.v1.api.comm.data;

import cloit.cloud.api.ncpapiservice.v1.api.compute.server.networkinterface.domain.request.NetworkInterfaceListRequest;
import cloit.cloud.api.ncpapiservice.v1.comm.domain.NcpApiKeyInfo;
import org.apache.commons.lang.StringUtils;

public class Validation {

    /**
     * NCP API 키 정보 유효성 검사
     * @param ncpApiKeyInfo NCP api key 정보
     */
    public static void apiKeyCheck(NcpApiKeyInfo ncpApiKeyInfo){
        final String accessKey = ncpApiKeyInfo.getAccessKey();
        final String secretKey = ncpApiKeyInfo.getSecretKey();

        if(StringUtils.isEmpty(accessKey)){
            throw new RuntimeException("Access Key is empty");
        }

        if(StringUtils.isEmpty(secretKey)){
            throw new RuntimeException("Secret Key is empty");
        }
    }

}
