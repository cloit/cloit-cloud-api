package cloit.cloud.api.ncpapiservice.v1.api.comm.domain;

import lombok.Data;

@Data
public class BlockDevicePartition {

    private String mountPoint;

    private String partitionSize;
}
