package cloit.cloud.api.ncpapiservice.v1.api.comm.domain;

import lombok.Data;

@Data
public class CommonCode {

    private String code;

    private String codeName;
}
