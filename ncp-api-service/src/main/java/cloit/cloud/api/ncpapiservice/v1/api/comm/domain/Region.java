package cloit.cloud.api.ncpapiservice.v1.api.comm.domain;

import lombok.Data;

@Data
public class Region {

    private String regionNo;

    private String regionCode;

    private String regionName;
}
