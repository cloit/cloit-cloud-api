package cloit.cloud.api.ncpapiservice.v1.api.comm.domain;

import lombok.Data;

@Data
public class Zone {

    private String zoneNo;

    private String zoneName;

    private String zoneCode;

    private String zoneDescription;

    private String regionNo;
}
