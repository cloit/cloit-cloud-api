package cloit.cloud.api.ncpapiservice.v1.api.compute.server.acg.domain.request;

import cloit.cloud.api.ncpapiservice.v1.comm.domain.NcpApiKeyInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang.StringUtils;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class AccessControlGroupListRequest extends NcpApiKeyInfo {

    private String regionCode;

    private String vpcNo;

    private List<String> accessControlGroupNoList;

    private String accessControlGroupName;

    private String accessControlGroupStatusCode;

    private Integer pageNo;

    private Integer pageSize;

    private String responseFormatType;

    public String getResponseFormatType() {
        return StringUtils.defaultString(responseFormatType, "json");
    }
}
