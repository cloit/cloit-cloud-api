package cloit.cloud.api.ncpapiservice.v1.api.compute.server.acg.domain.response;

import cloit.cloud.api.ncpapiservice.v1.api.comm.domain.CommonCode;
import lombok.Data;

@Data
public class AccessControlGroup {

    private String accessControlGroupNo;

    private String accessControlGroupName;

    private Boolean isDefault;

    private String vpcNo;

    private CommonCode accessControlGroupStatus;

    private String accessControlGroupDescription;
}
