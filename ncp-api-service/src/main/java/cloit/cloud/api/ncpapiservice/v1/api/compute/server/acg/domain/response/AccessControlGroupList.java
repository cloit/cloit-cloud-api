package cloit.cloud.api.ncpapiservice.v1.api.compute.server.acg.domain.response;

import lombok.Data;

import java.util.List;

@Data
public class AccessControlGroupList {

    private Integer totalRows;

    private List<AccessControlGroup> accessControlGroupList;
}
