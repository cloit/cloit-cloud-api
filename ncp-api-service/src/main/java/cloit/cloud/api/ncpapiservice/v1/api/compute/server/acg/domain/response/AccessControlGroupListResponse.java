package cloit.cloud.api.ncpapiservice.v1.api.compute.server.acg.domain.response;

import lombok.Data;

@Data
public class AccessControlGroupListResponse {

    private AccessControlGroupList getAccessControlGroupListResponse;
}
