package cloit.cloud.api.ncpapiservice.v1.api.compute.server.acg.service;

import cloit.cloud.api.core.component.http.domain.HttpRequest;
import cloit.cloud.api.ncpapiservice.v1.api.comm.data.Validation;
import cloit.cloud.api.ncpapiservice.v1.api.compute.server.acg.constants.AcgApiInfos;
import cloit.cloud.api.ncpapiservice.v1.api.compute.server.acg.domain.request.AccessControlGroupListRequest;
import cloit.cloud.api.ncpapiservice.v1.api.compute.server.acg.domain.response.AccessControlGroupListResponse;
import cloit.cloud.api.ncpapiservice.v1.comm.service.NcpHttpService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class AcgService {

    private final NcpHttpService ncpHttpService;

    /**
     * ACG 리스트 조회
     * @param accessControlGroupListRequest request 정보
     * @return AccessControlGroupListResponse
     */
    public AccessControlGroupListResponse getAccessControlGroupList(AccessControlGroupListRequest accessControlGroupListRequest){

        Validation.apiKeyCheck(accessControlGroupListRequest);

        final String host = AcgApiInfos.ACCESS_CONTROL_GROUP_LIST.getHost();
        final String path = AcgApiInfos.ACCESS_CONTROL_GROUP_LIST.getPath();

        final HttpRequest httpRequest = HttpRequest.create()
                .get()
                .setHost(host)
                .setPath(path)
                .setQueryParams(accessControlGroupListRequest);

        return  ncpHttpService.httpExecute(httpRequest, accessControlGroupListRequest, AccessControlGroupListResponse.class);
    }
}
