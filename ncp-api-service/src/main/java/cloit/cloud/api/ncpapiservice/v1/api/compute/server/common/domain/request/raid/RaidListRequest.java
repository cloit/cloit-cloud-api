package cloit.cloud.api.ncpapiservice.v1.api.compute.server.common.domain.request.raid;

import cloit.cloud.api.ncpapiservice.v1.comm.domain.NcpApiKeyInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang.StringUtils;

@EqualsAndHashCode(callSuper = true)
@Data
public class RaidListRequest extends NcpApiKeyInfo {

    private String productTypeCode;

    private String responseFormatType;

    public String getResponseFormatType() {
        return StringUtils.defaultString(responseFormatType, "json");
    }
}
