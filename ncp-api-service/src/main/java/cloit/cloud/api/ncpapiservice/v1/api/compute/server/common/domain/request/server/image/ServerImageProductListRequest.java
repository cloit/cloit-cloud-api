package cloit.cloud.api.ncpapiservice.v1.api.compute.server.common.domain.request.server.image;

import cloit.cloud.api.ncpapiservice.v1.comm.domain.NcpApiKeyInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang.StringUtils;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class ServerImageProductListRequest extends NcpApiKeyInfo {

    private String regionCode;

    private String blockStorageSize;

    private String exclusionProductCode;

    private String productCode;

    private List<String> platformTypeCodeList;

    private String infraResourceDetailTypeCode;

    private String responseFormatType;

    public String getResponseFormatType() {
        return StringUtils.defaultString(responseFormatType, "json");
    }
}
