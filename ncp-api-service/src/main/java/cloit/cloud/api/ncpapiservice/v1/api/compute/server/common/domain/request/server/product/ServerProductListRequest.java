package cloit.cloud.api.ncpapiservice.v1.api.compute.server.common.domain.request.server.product;

import cloit.cloud.api.ncpapiservice.v1.comm.domain.NcpApiKeyInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang.StringUtils;

@EqualsAndHashCode(callSuper = true)
@Data
public class ServerProductListRequest extends NcpApiKeyInfo{

    private String regionCode;

    private String zoneCode;

    private String serverImageProductCode;

    private String exclusionProductCode;

    private String productCode;

    private String generationCode;

    private String memberServerImageInstanceNo;

    private String responseFormatType;

    public String getResponseFormatType() {
        return StringUtils.defaultString(responseFormatType, "json");
    }
}
