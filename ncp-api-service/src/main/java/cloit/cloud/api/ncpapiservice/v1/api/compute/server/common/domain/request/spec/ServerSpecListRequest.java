package cloit.cloud.api.ncpapiservice.v1.api.compute.server.common.domain.request.spec;

import cloit.cloud.api.ncpapiservice.v1.comm.domain.NcpApiKeyInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang.StringUtils;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class ServerSpecListRequest extends NcpApiKeyInfo {

    private String regionCode;

    private String serverImageNo;

    private String zoneCode;

    private List<String> serverSpecCodeList;

    private List<String> hypervisorTypeCodeList;

    private String responseFormatType;

    public String getResponseFormatType() {
        return StringUtils.defaultString(responseFormatType, "json");
    }
}
