package cloit.cloud.api.ncpapiservice.v1.api.compute.server.common.domain.response.raid;

import cloit.cloud.api.ncpapiservice.v1.api.comm.domain.CommonCode;
import lombok.Data;

@Data
public class Raid {

    private String raidTypeName;

    private String raidName;

    private CommonCode productType;
}
