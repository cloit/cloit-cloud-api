package cloit.cloud.api.ncpapiservice.v1.api.compute.server.common.domain.response.raid;

import lombok.Data;

import java.util.List;

@Data
public class RaidList {

    private Integer totalRows;

    private List<Raid> raidList;
}
