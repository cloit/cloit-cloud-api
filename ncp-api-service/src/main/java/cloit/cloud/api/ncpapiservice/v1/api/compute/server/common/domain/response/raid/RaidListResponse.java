package cloit.cloud.api.ncpapiservice.v1.api.compute.server.common.domain.response.raid;

import lombok.Data;

@Data
public class RaidListResponse {

    private RaidList getRaidListResponse;
}
