package cloit.cloud.api.ncpapiservice.v1.api.compute.server.common.domain.response.region;

import lombok.Data;

@Data
public class ServerRegion {

    private String regionCode;

    private String regionName;
}
