package cloit.cloud.api.ncpapiservice.v1.api.compute.server.common.domain.response.region;

import lombok.Data;

import java.util.List;

@Data
public class ServerRegionList {

    private Integer totalRows;

    private List<ServerRegion> regionList;
}
