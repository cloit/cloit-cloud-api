package cloit.cloud.api.ncpapiservice.v1.api.compute.server.common.domain.response.region;

import lombok.Data;

@Data
public class ServerRegionListResponse {

    private ServerRegionList getRegionListResponse;
}
