package cloit.cloud.api.ncpapiservice.v1.api.compute.server.common.domain.response.server.comm;

import lombok.Data;

import java.util.List;

@Data
public class ProductList {

    private Integer totalRows;

    private List<Product> productList;
}
