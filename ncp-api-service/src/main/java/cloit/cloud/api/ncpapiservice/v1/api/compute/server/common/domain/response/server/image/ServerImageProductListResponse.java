package cloit.cloud.api.ncpapiservice.v1.api.compute.server.common.domain.response.server.image;

import cloit.cloud.api.ncpapiservice.v1.api.compute.server.common.domain.response.server.comm.ProductList;
import lombok.Data;

@Data
public class ServerImageProductListResponse {

    private ProductList getServerImageProductListResponse;
}
