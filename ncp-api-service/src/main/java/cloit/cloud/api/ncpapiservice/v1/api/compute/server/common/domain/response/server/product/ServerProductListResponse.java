package cloit.cloud.api.ncpapiservice.v1.api.compute.server.common.domain.response.server.product;

import cloit.cloud.api.ncpapiservice.v1.api.compute.server.common.domain.response.server.comm.ProductList;
import lombok.Data;

@Data
public class ServerProductListResponse {

    private ProductList getServerProductListResponse;
}
