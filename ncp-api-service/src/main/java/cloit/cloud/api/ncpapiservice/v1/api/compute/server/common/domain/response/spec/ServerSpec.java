package cloit.cloud.api.ncpapiservice.v1.api.compute.server.common.domain.response.spec;

import cloit.cloud.api.ncpapiservice.v1.api.comm.domain.CommonCode;
import lombok.Data;

@Data
public class ServerSpec {

    private String serverSpecCode;

    private CommonCode hypervisorType;

    private CommonCode cpuArchitectureType;

    private String generationCode;

    private Integer cpuCount;

    private Long memorySize;

    private Integer blockStorageMaxCount;

    private Long blockStorageMaxIops;

    private Long blockStorageMaxThroughput;

    private Long networkPerformance;

    private Integer networkInterfaceMaxCount;

    private Integer gpuCount;

    private String serverSpecDescription;

    private String serverProductCode;
}
