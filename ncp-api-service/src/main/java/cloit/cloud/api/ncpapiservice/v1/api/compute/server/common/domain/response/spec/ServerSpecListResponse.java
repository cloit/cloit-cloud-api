package cloit.cloud.api.ncpapiservice.v1.api.compute.server.common.domain.response.spec;

import lombok.Data;

@Data
public class ServerSpecListResponse {

    private ServerSpecList getServerSpecListResponse;
}
