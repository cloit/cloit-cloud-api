package cloit.cloud.api.ncpapiservice.v1.api.compute.server.common.service;

import cloit.cloud.api.core.component.http.domain.HttpRequest;
import cloit.cloud.api.ncpapiservice.v1.api.comm.data.Validation;
import cloit.cloud.api.ncpapiservice.v1.api.compute.server.common.constants.ServerCommonApiInfos;
import cloit.cloud.api.ncpapiservice.v1.api.compute.server.common.domain.request.raid.RaidListRequest;
import cloit.cloud.api.ncpapiservice.v1.api.compute.server.common.domain.request.server.image.ServerImageProductListRequest;
import cloit.cloud.api.ncpapiservice.v1.api.compute.server.common.domain.request.server.product.ServerProductListRequest;
import cloit.cloud.api.ncpapiservice.v1.api.compute.server.common.domain.request.spec.ServerSpecListRequest;
import cloit.cloud.api.ncpapiservice.v1.api.compute.server.common.domain.response.raid.RaidListResponse;
import cloit.cloud.api.ncpapiservice.v1.api.compute.server.common.domain.response.region.ServerRegionListResponse;
import cloit.cloud.api.ncpapiservice.v1.api.compute.server.common.domain.response.server.image.ServerImageProductListResponse;
import cloit.cloud.api.ncpapiservice.v1.api.compute.server.common.domain.response.server.product.ServerProductListResponse;
import cloit.cloud.api.ncpapiservice.v1.api.compute.server.common.domain.response.spec.ServerSpecListResponse;
import cloit.cloud.api.ncpapiservice.v1.api.compute.server.serverimage.domain.request.server.ServerImageListRequest;
import cloit.cloud.api.ncpapiservice.v1.comm.domain.NcpApiKeyInfo;
import cloit.cloud.api.ncpapiservice.v1.comm.service.NcpHttpService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class ServerCommonService {

    private final NcpHttpService ncpHttpService;

    /**
     * 서버 리전 조회
     * @param ncpApiKeyInfo Ncp api key 정보
     * @return ServerRegionListResponse
     */
    public ServerRegionListResponse getRegionList(NcpApiKeyInfo ncpApiKeyInfo){

        Validation.apiKeyCheck(ncpApiKeyInfo);

        final String host = ServerCommonApiInfos.SERVER_REGION_LIST.getHost();
        final String path = ServerCommonApiInfos.SERVER_REGION_LIST.getPath();

        final HttpRequest httpRequest = HttpRequest.create()
                .get()
                .setHost(host)
                .setPath(path)
                .setQueryParams("responseFormatType", "json");


        return ncpHttpService.httpExecute(httpRequest, ncpApiKeyInfo, ServerRegionListResponse.class);

    }

    /**
     * 서버 이미지 product code 조회
     * @param serverImageListRequest request 정보
     * @return ServerImageProductListResponse
     */
    public ServerImageProductListResponse getServerImageProductList(ServerImageListRequest serverImageListRequest){

        Validation.apiKeyCheck(serverImageListRequest);

        final String host = ServerCommonApiInfos.SERVER_IMAGE_PRODUCT_LIST.getHost();
        final String path = ServerCommonApiInfos.SERVER_IMAGE_PRODUCT_LIST.getPath();
        final ServerImageProductListRequest serverImageProductListRequest = new ServerImageProductListRequest();

        final HttpRequest httpRequest = HttpRequest.create()
                .get()
                .setHost(host)
                .setPath(path)
                .setQueryParams(serverImageProductListRequest);

        return ncpHttpService.httpExecute(httpRequest, serverImageListRequest, ServerImageProductListResponse.class);
    }

    /**
     * Server 상품 조회
     * @param serverProductListRequest request 정보
     * @return ServerProductListResponse
     */
    public ServerProductListResponse getServerProductList(ServerProductListRequest serverProductListRequest){

        Validation.apiKeyCheck(serverProductListRequest);

        final String host = ServerCommonApiInfos.SERVER_PRODUCT_LIST.getHost();
        final String path = ServerCommonApiInfos.SERVER_PRODUCT_LIST.getPath();

        final HttpRequest httpRequest = HttpRequest.create()
                .get()
                .setHost(host)
                .setPath(path)
                .setQueryParams(serverProductListRequest);

        return ncpHttpService.httpExecute(httpRequest, serverProductListRequest, ServerProductListResponse.class);
    }

    /**
     * Server 스펙 리스트 조회
     * @param serverSpecListRequest request 정보
     * @return ServerSpecListResponse
     */
    public ServerSpecListResponse getServerSpecList(ServerSpecListRequest serverSpecListRequest){

        Validation.apiKeyCheck(serverSpecListRequest);

        final String host = ServerCommonApiInfos.SERVER_SPEC_LIST.getHost();
        final String path = ServerCommonApiInfos.SERVER_SPEC_LIST.getPath();

        final HttpRequest httpRequest = HttpRequest.create()
                .get()
                .setHost(host)
                .setPath(path)
                .setQueryParams(serverSpecListRequest);

        return ncpHttpService.httpExecute(httpRequest, serverSpecListRequest, ServerSpecListResponse.class);
    }

    /**
     * 사용 가능한 RAID 리스트 조회
     * @param raidListRequest request 정보
     * @return RaidListResponse
     */
    public RaidListResponse getRaidList(RaidListRequest raidListRequest){

        Validation.apiKeyCheck(raidListRequest);

        final String host = ServerCommonApiInfos.RAID_LIST.getHost();
        final String path = ServerCommonApiInfos.RAID_LIST.getPath();

        final HttpRequest httpRequest = HttpRequest.create()
                .get()
                .setHost(host)
                .setPath(path)
                .setQueryParams(raidListRequest);

        return ncpHttpService.httpExecute(httpRequest, raidListRequest, RaidListResponse.class);
    }
}
