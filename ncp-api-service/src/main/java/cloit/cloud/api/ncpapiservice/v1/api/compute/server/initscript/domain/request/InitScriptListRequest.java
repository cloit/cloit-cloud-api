package cloit.cloud.api.ncpapiservice.v1.api.compute.server.initscript.domain.request;

import cloit.cloud.api.ncpapiservice.v1.comm.domain.NcpApiKeyInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang.StringUtils;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class InitScriptListRequest extends NcpApiKeyInfo {

    private String regionCode;

    private List<String> initScriptNoList;

    private String initScriptName;

    private String osTypeCode;

    private Integer pageNo;

    private Integer pageSize;

    private String sortedBy;

    private String sortingOrder;

    private String responseFormatType;

    public String getResponseFormatType() {
        return StringUtils.defaultString(responseFormatType, "json");
    }
}
