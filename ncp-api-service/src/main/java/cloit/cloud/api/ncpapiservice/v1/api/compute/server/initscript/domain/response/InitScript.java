package cloit.cloud.api.ncpapiservice.v1.api.compute.server.initscript.domain.response;

import cloit.cloud.api.ncpapiservice.v1.api.comm.domain.CommonCode;
import lombok.Data;

import java.util.Date;

@Data
public class InitScript {

    private String initScriptNo;

    private String initScriptName;

    private Date createDate;

    private String initScriptDescription;

    private String initScriptContent;

    private CommonCode osType;
}
