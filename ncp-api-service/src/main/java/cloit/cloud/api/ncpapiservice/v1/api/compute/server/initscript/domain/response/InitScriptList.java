package cloit.cloud.api.ncpapiservice.v1.api.compute.server.initscript.domain.response;

import lombok.Data;

import java.util.List;

@Data
public class InitScriptList {

    private Integer totalRows;

    private List<InitScript> initScriptList;
}
