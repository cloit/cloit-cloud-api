package cloit.cloud.api.ncpapiservice.v1.api.compute.server.initscript.domain.response;

import lombok.Data;

@Data
public class InitScriptListResponse {

    private InitScriptList getInitScriptListResponse;
}
