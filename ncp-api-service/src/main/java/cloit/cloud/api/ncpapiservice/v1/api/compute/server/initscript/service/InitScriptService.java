package cloit.cloud.api.ncpapiservice.v1.api.compute.server.initscript.service;

import cloit.cloud.api.core.component.http.domain.HttpRequest;
import cloit.cloud.api.ncpapiservice.v1.api.comm.data.Validation;
import cloit.cloud.api.ncpapiservice.v1.api.compute.server.initscript.constants.InitScriptApiInfos;
import cloit.cloud.api.ncpapiservice.v1.api.compute.server.initscript.domain.request.InitScriptListRequest;
import cloit.cloud.api.ncpapiservice.v1.api.compute.server.initscript.domain.response.InitScriptListResponse;
import cloit.cloud.api.ncpapiservice.v1.comm.service.NcpHttpService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class InitScriptService {

    private final NcpHttpService ncpHttpService;

    /**
     * 초기화 스크립트 리스트 조회
     * @param initScriptListRequest request 정보
     * @return InitScriptListResponse
     */
    public InitScriptListResponse getInitScriptList(InitScriptListRequest initScriptListRequest){

        Validation.apiKeyCheck(initScriptListRequest);

        final String host = InitScriptApiInfos.INIT_SCRIPT_LIST.getHost();
        final String path = InitScriptApiInfos.INIT_SCRIPT_LIST.getPath();

        final HttpRequest httpRequest = HttpRequest.create()
                .get()
                .setHost(host)
                .setPath(path)
                .setQueryParams(initScriptListRequest);

        return ncpHttpService.httpExecute(httpRequest, initScriptListRequest, InitScriptListResponse.class);
    }
}
