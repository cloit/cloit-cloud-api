package cloit.cloud.api.ncpapiservice.v1.api.compute.server.initscript.web;

import cloit.cloud.api.ncpapiservice.v1.api.comm.data.ResponseData;
import cloit.cloud.api.ncpapiservice.v1.api.compute.server.initscript.domain.request.InitScriptListRequest;
import cloit.cloud.api.ncpapiservice.v1.api.compute.server.initscript.domain.response.InitScriptListResponse;
import cloit.cloud.api.ncpapiservice.v1.api.compute.server.initscript.service.InitScriptService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.Map;

@RestController
@RequestMapping("/ncp/vpc/server")
@RequiredArgsConstructor
@Slf4j
public class InitScriptController {

    private final InitScriptService initScriptService;

    /**
     * 초기화 스크립트 리스트 조회
     * @param initScriptListRequest request 정보
     * @return Mono<Map<String, Object>>
     */
    @RequestMapping("/initScripts")
    public Mono<Map<String, Object>> initScripts(@RequestBody InitScriptListRequest initScriptListRequest){
        log.error("##### initScriptListRequest : {}", initScriptListRequest.toString());
        final ResponseData responseData = new ResponseData();
        try{
            final InitScriptListResponse initScriptListResponse
                    = initScriptService.getInitScriptList(initScriptListRequest);
            responseData.responseSuccess(initScriptListResponse);
        }catch (Exception e){
            log.debug("##### InitScripts Exception", e);
            responseData.responseFail(e);
        }
        return Mono.just(responseData.responseResult());
    }
}
