package cloit.cloud.api.ncpapiservice.v1.api.compute.server.networkinterface.domain.request;

import cloit.cloud.api.ncpapiservice.v1.comm.domain.NcpApiKeyInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang.StringUtils;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class NetworkInterfaceListRequest extends NcpApiKeyInfo {

    private String regionCode;

    private String subnetName;

    private List<String> networkInterfaceNoList;

    private String networkInterfaceName;

    private String networkInterfaceStatusCode;

    private String ip;

    private List<String> secondaryIpList;

    private String instanceNo;

    private Boolean isDefault;

    private String deviceName;

    private String serverName;

    private Integer pageNo;

    private Integer pageSize;

    private String responseFormatType;

    public String getResponseFormatType() {
        return StringUtils.defaultString(responseFormatType, "json");
    }
}
