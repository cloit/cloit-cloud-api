package cloit.cloud.api.ncpapiservice.v1.api.compute.server.networkinterface.domain.response;

import lombok.Data;

import java.util.List;

@Data
public class NetworkInterfaceList {

    private Integer totalRows;

    private List<NetworkInterface> networkInterfaceList;
}
