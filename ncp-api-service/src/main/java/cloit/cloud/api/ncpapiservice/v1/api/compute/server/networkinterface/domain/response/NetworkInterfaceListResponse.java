package cloit.cloud.api.ncpapiservice.v1.api.compute.server.networkinterface.domain.response;

import lombok.Data;

@Data
public class NetworkInterfaceListResponse {

    private NetworkInterfaceList getNetworkInterfaceListResponse;
}
