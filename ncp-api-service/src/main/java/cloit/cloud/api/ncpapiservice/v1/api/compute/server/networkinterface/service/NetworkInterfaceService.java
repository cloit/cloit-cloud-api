package cloit.cloud.api.ncpapiservice.v1.api.compute.server.networkinterface.service;

import cloit.cloud.api.core.component.http.domain.HttpRequest;
import cloit.cloud.api.ncpapiservice.v1.api.comm.data.Validation;
import cloit.cloud.api.ncpapiservice.v1.api.compute.server.networkinterface.constants.NetworkInterfaceApiInfos;
import cloit.cloud.api.ncpapiservice.v1.api.compute.server.networkinterface.domain.request.NetworkInterfaceListRequest;
import cloit.cloud.api.ncpapiservice.v1.api.compute.server.networkinterface.domain.response.NetworkInterfaceListResponse;
import cloit.cloud.api.ncpapiservice.v1.comm.service.NcpHttpService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class NetworkInterfaceService {

    private final NcpHttpService ncpHttpService;

    /**
     * 네트워크 인터페이스 리스트 조회
     * @param networkInterfaceListRequest request 정보
     * @return NetworkInterfaceListResponse
     */
    public NetworkInterfaceListResponse getNetworkInterfaceList(NetworkInterfaceListRequest networkInterfaceListRequest){

        Validation.apiKeyCheck(networkInterfaceListRequest);

        final String host = NetworkInterfaceApiInfos.VPC_NETWORK_INTERFACE_LIST.getHost();
        final String path = NetworkInterfaceApiInfos.VPC_NETWORK_INTERFACE_LIST.getPath();

        final HttpRequest httpRequest = HttpRequest.create()
                .get()
                .setHost(host)
                .setPath(path)
                .setQueryParams(networkInterfaceListRequest);

        return ncpHttpService.httpExecute(httpRequest, networkInterfaceListRequest, NetworkInterfaceListResponse.class);

    }
}
