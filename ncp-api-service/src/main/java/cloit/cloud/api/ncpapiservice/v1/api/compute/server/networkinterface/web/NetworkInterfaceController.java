package cloit.cloud.api.ncpapiservice.v1.api.compute.server.networkinterface.web;

import cloit.cloud.api.ncpapiservice.v1.api.comm.data.ResponseData;
import cloit.cloud.api.ncpapiservice.v1.api.compute.server.networkinterface.domain.request.NetworkInterfaceListRequest;
import cloit.cloud.api.ncpapiservice.v1.api.compute.server.networkinterface.domain.response.NetworkInterfaceListResponse;
import cloit.cloud.api.ncpapiservice.v1.api.compute.server.networkinterface.service.NetworkInterfaceService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.Map;

@RestController
@RequestMapping("/ncp/vpc/server")
@RequiredArgsConstructor
@Slf4j
public class NetworkInterfaceController {

    private final NetworkInterfaceService networkInterfaceService;

    /**
     * 네트워크 인터페이스 리스트 조회
     * @param networkInterfaceListRequest request 정보
     * @return Mono<Map<String, Object>>
     */
    @RequestMapping("/network/interfaces")
    public Mono<Map<String, Object>> networkInterfaces(@RequestBody NetworkInterfaceListRequest networkInterfaceListRequest){
        log.debug("##### networkInterfaceListRequest : {}", networkInterfaceListRequest.toString());
        final ResponseData responseData = new ResponseData();
        try{
            final NetworkInterfaceListResponse networkInterfaceListResponse
                    = networkInterfaceService.getNetworkInterfaceList(networkInterfaceListRequest);
            responseData.responseSuccess(networkInterfaceListResponse);
        }catch (Exception e){
            log.debug("##### Network Interfaces Exception ", e);
            responseData.responseFail(e);
        }
        return Mono.just(responseData.responseResult());
    }
}
