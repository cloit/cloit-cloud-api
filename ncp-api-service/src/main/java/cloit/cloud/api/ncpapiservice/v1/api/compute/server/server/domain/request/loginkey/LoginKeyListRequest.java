package cloit.cloud.api.ncpapiservice.v1.api.compute.server.server.domain.request.loginkey;

import cloit.cloud.api.ncpapiservice.v1.comm.domain.NcpApiKeyInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang.StringUtils;

@EqualsAndHashCode(callSuper = true)
@Data
public class LoginKeyListRequest extends NcpApiKeyInfo {

    private String keyName;

    private Integer pageNo;

    private Integer pageSize;

    private String responseFormatType;

    public String getResponseFormatType() {
        return StringUtils.defaultString(responseFormatType, "json");
    }
}
