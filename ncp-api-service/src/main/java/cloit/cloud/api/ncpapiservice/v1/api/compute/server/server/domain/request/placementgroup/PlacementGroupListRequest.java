package cloit.cloud.api.ncpapiservice.v1.api.compute.server.server.domain.request.placementgroup;

import cloit.cloud.api.ncpapiservice.v1.comm.domain.NcpApiKeyInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang.StringUtils;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class PlacementGroupListRequest extends NcpApiKeyInfo {

    private String regionCode;

    private List<String> placementGroupNoList;

    private String placementGroupName;

    private String responseFormatType;

    public String getResponseFormatType() {
        return StringUtils.defaultString(responseFormatType, "json");
    }
}
