package cloit.cloud.api.ncpapiservice.v1.api.compute.server.server.domain.request.server;

import cloit.cloud.api.ncpapiservice.v1.api.comm.domain.BlockDevicePartition;
import cloit.cloud.api.ncpapiservice.v1.api.comm.domain.BlockStorageMapping;
import cloit.cloud.api.ncpapiservice.v1.comm.domain.NcpApiKeyInfo;
import lombok.*;
import org.apache.commons.lang.StringUtils;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class CreateServerInstanceRequest extends NcpApiKeyInfo {

    private String regionCode;

    private String memberServerImageInstanceNo;

    private String serverImageProductCode;

    private String serverImageNo;

    private String vpcNo;

    private String subnetNo;

    private String serverProductCode;

    private String serverSpecCode;

    private Boolean isEncryptedBaseBlockStorageVolume;

    private String feeSystemTypeCode;

    private Integer serverCreateCount;

    private Integer serverCreateStartNo;

    private String serverName;

    private List<CreateServerNetworkInterface> networkInterfaceList;

    private String placementGroupNo;

    private Boolean isProtectServerTermination;

    private String serverDescription;

    private String initScriptNo;

    private String loginKeyName;

    private Boolean associateWithPublicIp;

    private String raidTypeName;

    private List<BlockDevicePartition> blockDevicePartitionList;

    private List<BlockStorageMapping> blockStorageMappingList;

    private String responseFormatType;

    public String getResponseFormatType() {
        return StringUtils.defaultString(responseFormatType, "json");
    }
}
