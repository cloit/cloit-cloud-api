package cloit.cloud.api.ncpapiservice.v1.api.compute.server.server.domain.request.server;

import cloit.cloud.api.ncpapiservice.v1.comm.domain.NcpApiKeyInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang.StringUtils;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class StartServerInstanceRequest extends NcpApiKeyInfo {

    private String regionCode;

    private List<String> serverInstanceNoList;

    private String responseFormatType;

    public String getResponseFormatType() {
        return StringUtils.defaultString(responseFormatType, "json");
    }
}
