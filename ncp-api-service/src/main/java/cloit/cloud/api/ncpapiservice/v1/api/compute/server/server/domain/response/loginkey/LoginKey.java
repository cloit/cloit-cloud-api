package cloit.cloud.api.ncpapiservice.v1.api.compute.server.server.domain.response.loginkey;

import lombok.Data;

import java.util.Date;

@Data
public class LoginKey {

    private String fingerprint;

    private String keyName;

    private Date createDate;
}
