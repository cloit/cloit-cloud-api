package cloit.cloud.api.ncpapiservice.v1.api.compute.server.server.domain.response.loginkey;

import lombok.Data;

import java.util.List;

@Data
public class LoginKeyList {

    private Integer totalRows;

    private List<LoginKey> loginKeyList;
}
