package cloit.cloud.api.ncpapiservice.v1.api.compute.server.server.domain.response.loginkey;

import lombok.Data;

@Data
public class LoginKeyListResponse {

    private LoginKeyList getLoginKeyListResponse;
}
