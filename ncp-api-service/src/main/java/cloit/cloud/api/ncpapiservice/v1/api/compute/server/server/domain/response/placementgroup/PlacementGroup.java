package cloit.cloud.api.ncpapiservice.v1.api.compute.server.server.domain.response.placementgroup;

import cloit.cloud.api.ncpapiservice.v1.api.comm.domain.CommonCode;
import lombok.Data;

@Data
public class PlacementGroup {

    private String placementGroupNo;

    private String placementGroupName;

    private CommonCode placementGroupType;
}
