package cloit.cloud.api.ncpapiservice.v1.api.compute.server.server.domain.response.placementgroup;

import lombok.Data;

import java.util.List;

@Data
public class PlacementGroupList {

    private Integer totalRows;

    private List<PlacementGroup> placementGroupList;
}
