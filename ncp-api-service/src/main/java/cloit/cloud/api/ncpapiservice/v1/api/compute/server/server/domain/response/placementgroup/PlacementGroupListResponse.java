package cloit.cloud.api.ncpapiservice.v1.api.compute.server.server.domain.response.placementgroup;

import lombok.Data;

@Data
public class PlacementGroupListResponse {

    private PlacementGroupList getPlacementGroupListResponse;
}
