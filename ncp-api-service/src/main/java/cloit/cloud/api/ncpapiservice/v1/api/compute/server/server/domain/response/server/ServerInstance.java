package cloit.cloud.api.ncpapiservice.v1.api.compute.server.server.domain.response.server;

import cloit.cloud.api.ncpapiservice.v1.api.comm.domain.CommonCode;
import cloit.cloud.api.ncpapiservice.v1.api.comm.domain.BlockDevicePartition;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class ServerInstance {

    private String serverInstanceNo;

    private String serverName;

    private String serverDescription;

    private Integer cpuCount;

    private Long memorySize;

    private CommonCode platformType;

    private String loginKeyName;

    private String publicIpInstanceNo;

    private String publicIp;

    private CommonCode serverInstanceStatus;

    private CommonCode serverInstanceOperation;

    private String serverInstanceStatusName;

    private Date createDate;

    private Date uptime;

    private String serverImageProductCode;

    private String serverProductCode;

    private Boolean isProtectServerTermination;

    private String zoneCode;

    private String regionCode;

    private String vpcNo;

    private String subnetNo;

    private List<String> networkInterfaceNoList;

    private String initScriptNo;

    private CommonCode serverInstanceType;

    private CommonCode baseBlockStorageDiskType;

    private CommonCode baseBlockStorageDiskDetailType;

    private String placementGroupNo;

    private String placementGroupName;

    private String memberServerImageInstanceNo;

    private List<BlockDevicePartition> blockDevicePartitionList;

    private CommonCode hypervisorType;

    private String serverImageNo;

    private String serverSpecCode;
}
