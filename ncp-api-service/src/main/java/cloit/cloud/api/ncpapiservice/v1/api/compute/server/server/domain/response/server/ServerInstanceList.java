package cloit.cloud.api.ncpapiservice.v1.api.compute.server.server.domain.response.server;

import lombok.Data;

import java.util.List;

@Data
public class ServerInstanceList {

    private Integer totalRows;

    private List<ServerInstance> serverInstanceList;

}
