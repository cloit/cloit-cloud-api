package cloit.cloud.api.ncpapiservice.v1.api.compute.server.server.domain.response.server;

import lombok.Data;

@Data
public class StartServerInstanceResponse {

    private ServerInstanceList startServerInstancesResponse;
}
