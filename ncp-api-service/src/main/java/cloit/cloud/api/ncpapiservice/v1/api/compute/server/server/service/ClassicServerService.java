package cloit.cloud.api.ncpapiservice.v1.api.compute.server.server.service;

import cloit.cloud.api.core.component.http.domain.HttpRequest;
import cloit.cloud.api.ncpapiservice.v1.api.comm.data.Validation;
import cloit.cloud.api.ncpapiservice.v1.api.compute.server.server.constants.ClassicServerApiInfos;
import cloit.cloud.api.ncpapiservice.v1.api.compute.server.server.domain.request.server.ServerInstanceListRequest;
import cloit.cloud.api.ncpapiservice.v1.api.compute.server.server.domain.response.server.ServerInstanceListResponse;
import cloit.cloud.api.ncpapiservice.v1.comm.service.NcpHttpService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class ClassicServerService {

    private final NcpHttpService ncpHttpService;

    /**
     * 서버(classic) 인스턴스 조회
     * @param serverInstanceListRequest request 정보
     * @return ServerInstanceListResponse
     */
    public ServerInstanceListResponse getServerInstanceList(ServerInstanceListRequest serverInstanceListRequest){

        Validation.apiKeyCheck(serverInstanceListRequest);

        final String host = ClassicServerApiInfos.CLASSIC_SERVER_INSTANCE_LIST.getHost();
        final String path = ClassicServerApiInfos.CLASSIC_SERVER_INSTANCE_LIST.getPath();

        final HttpRequest httpRequest = HttpRequest.create()
                .get()
                .setHost(host)
                .setPath(path)
                .setQueryParams(serverInstanceListRequest);

        return ncpHttpService.httpExecute(httpRequest, serverInstanceListRequest, ServerInstanceListResponse.class);
    }
}
