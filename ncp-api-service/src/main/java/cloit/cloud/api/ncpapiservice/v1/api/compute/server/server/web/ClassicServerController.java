package cloit.cloud.api.ncpapiservice.v1.api.compute.server.server.web;

import cloit.cloud.api.ncpapiservice.v1.api.comm.data.ResponseData;
import cloit.cloud.api.ncpapiservice.v1.api.compute.server.server.domain.request.server.ServerInstanceListRequest;
import cloit.cloud.api.ncpapiservice.v1.api.compute.server.server.domain.response.server.ServerInstanceListResponse;
import cloit.cloud.api.ncpapiservice.v1.api.compute.server.server.service.ClassicServerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.Map;

@RestController
@RequestMapping("/ncp/classic")
@RequiredArgsConstructor
@Slf4j
public class ClassicServerController {

    private final ClassicServerService classicServerService;

    /**
     * classic 서버 인스턴스 정보
     * @param serverInstanceListRequest request 정보
     * @return Mono<Map<String, Object>>
     */
    @RequestMapping("/server/instances")
    public Mono<Map<String, Object>> serverInstances(@RequestBody ServerInstanceListRequest serverInstanceListRequest){
        log.debug("###### serverInstanceListRequest : {}", serverInstanceListRequest.toString());
        ResponseData responseData = new ResponseData();
        try{
            ServerInstanceListResponse serverInstanceListResponse = classicServerService.getServerInstanceList(serverInstanceListRequest);
            responseData.responseSuccess(serverInstanceListResponse);
        }catch (Exception e){
            log.debug("##### Classic ServerInstances Exception", e);
            responseData.responseFail(e);
        }
        return Mono.just(responseData.responseResult());
    }
}
