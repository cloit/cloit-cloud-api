package cloit.cloud.api.ncpapiservice.v1.api.compute.server.server.web;

import cloit.cloud.api.ncpapiservice.v1.api.comm.data.ResponseData;
import cloit.cloud.api.ncpapiservice.v1.api.compute.server.server.domain.request.loginkey.LoginKeyListRequest;
import cloit.cloud.api.ncpapiservice.v1.api.compute.server.server.domain.request.placementgroup.PlacementGroupListRequest;
import cloit.cloud.api.ncpapiservice.v1.api.compute.server.server.domain.request.server.CreateServerInstanceRequest;
import cloit.cloud.api.ncpapiservice.v1.api.compute.server.server.domain.request.server.ServerInstanceListRequest;
import cloit.cloud.api.ncpapiservice.v1.api.compute.server.server.domain.request.server.StartServerInstanceRequest;
import cloit.cloud.api.ncpapiservice.v1.api.compute.server.server.domain.response.loginkey.LoginKeyListResponse;
import cloit.cloud.api.ncpapiservice.v1.api.compute.server.server.domain.response.placementgroup.PlacementGroupListResponse;
import cloit.cloud.api.ncpapiservice.v1.api.compute.server.server.domain.response.server.CreateServerInstanceResponse;
import cloit.cloud.api.ncpapiservice.v1.api.compute.server.server.domain.response.server.ServerInstanceListResponse;
import cloit.cloud.api.ncpapiservice.v1.api.compute.server.server.domain.response.server.StartServerInstanceResponse;
import cloit.cloud.api.ncpapiservice.v1.api.compute.server.server.service.VpcServerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.Map;

@RestController
@RequestMapping("/ncp/vpc/server")
@RequiredArgsConstructor
@Slf4j
public class VpcServerController {

    private final VpcServerService vpcServerService;

    /**
     * vpc 서버 인스터스 조회
     * @param serverInstanceListRequest request 정보
     * @return Mono<Map<String, Object>>
     */
    @RequestMapping("/instances")
    public Mono<Map<String, Object>> serverInstances(@RequestBody ServerInstanceListRequest serverInstanceListRequest){
        log.debug("##### serverInstanceListRequest : {}", serverInstanceListRequest.toString());
        final ResponseData responseData = new ResponseData();
        try{
            ServerInstanceListResponse serverInstanceListResponse = vpcServerService.getServerInstanceList(serverInstanceListRequest);
            responseData.responseSuccess(serverInstanceListResponse);
        }catch (Exception e){
            log.debug("##### Vpc ServerInstances Exception", e);
            responseData.responseFail(e);
        }
        return Mono.just(responseData.responseResult());
    }

    /**
     * 서버 인스턴스가 소속되는 물리 배치 그룹 리스트 조회
     * @param placementGroupListRequest request 정보
     * @return Mono<Map<String, Object>>
     */
    @RequestMapping("/placement/groups")
    public Mono<Map<String, Object>> placementGroups(@RequestBody PlacementGroupListRequest placementGroupListRequest){
        log.debug("##### placementGroupListRequest : {}", placementGroupListRequest.toString());
        final ResponseData responseData = new ResponseData();
        try{
            final PlacementGroupListResponse placementGroupListResponse
                    = vpcServerService.getPlacementGroupList(placementGroupListRequest);
            responseData.responseSuccess(placementGroupListResponse);
        }catch (Exception e){
            log.debug("##### Placement Groups Exception ", e);
            responseData.responseFail(e);
        }
        return Mono.just(responseData.responseResult());
    }

    /**
     * 복호화 키 조회
     * @param loginKeyListRequest request 정보
     * @return Mono<Map<String, Object>>
     */
    @RequestMapping("/login/keys")
    public Mono<Map<String, Object>> loginKeys(@RequestBody LoginKeyListRequest loginKeyListRequest){
        log.debug("##### loginKeyListRequest : {}", loginKeyListRequest.toString());
        final ResponseData responseData = new ResponseData();
        try{
            final LoginKeyListResponse loginKeyListResponse
                    = vpcServerService.getLoginKeyList(loginKeyListRequest);
            responseData.responseSuccess(loginKeyListResponse);
        }catch (Exception e){
            log.debug("##### Login Keys Exception ", e);
            responseData.responseFail(e);
        }
        return Mono.just(responseData.responseResult());
    }

    /**
     * 서버 인스턴스 생성
     * @param createServerInstanceRequest request 정보
     * @return Mono<Map<String, Object>>
     */
    @RequestMapping("/instances/create")
    public Mono<Map<String, Object>> serverInstancesCreate(@RequestBody CreateServerInstanceRequest createServerInstanceRequest){
        log.debug("##### createServerInstanceRequest : {}", createServerInstanceRequest.toString());
        final ResponseData responseData = new ResponseData();
        try{
            final CreateServerInstanceResponse createServerInstanceResponse
                    = vpcServerService.createServerInstances(createServerInstanceRequest);
            responseData.responseSuccess(createServerInstanceResponse);
//            responseData.responseSuccess(RequestDataConverter.requestDataConverter(createServerInstanceRequest));
        }catch (Exception e){
            log.debug("##### Create Server Exception ", e);
            responseData.responseFail(e);
        }
        return Mono.just(responseData.responseResult());
    }

    /**
     * 서버 인스턴스 시작
     * @param startServerInstanceRequest request 정보
     * @return Mono<Map<String, Object>>
     */
    @RequestMapping("/instances/start")
    public Mono<Map<String, Object>> serverInstancesStart(@RequestBody StartServerInstanceRequest startServerInstanceRequest){
        log.debug("##### startServerInstanceRequest : {}", startServerInstanceRequest.toString());
        final ResponseData responseData = new ResponseData();
        try{
            final StartServerInstanceResponse startServerInstanceResponse
                    = vpcServerService.startServerInstance(startServerInstanceRequest);
            responseData.responseSuccess(startServerInstanceResponse);
        }catch (Exception e){
            log.debug("##### Server Instances Start Exception ", e);
            responseData.responseFail(e);
        }
        return Mono.just(responseData.responseResult());
    }
}
