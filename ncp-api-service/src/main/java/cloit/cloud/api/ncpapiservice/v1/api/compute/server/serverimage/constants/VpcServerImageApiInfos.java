package cloit.cloud.api.ncpapiservice.v1.api.compute.server.serverimage.constants;

import lombok.Getter;

public enum VpcServerImageApiInfos {

    VPC_MEMBER_SERVER_IMAGE_INSTANCE_LIST(
            "VPC_MEMBER_SERVER_IMAGE_INSTANCE_LIST"
            , "회원 서버 이미지 인스턴스 목록"
            , "https://ncloud.apigw.ntruss.com"
            , "/vserver/v2/getMemberServerImageInstanceList"
            , "GET"
    ),
    VPC_SERVER_IMAGE_LIST(
            "VPC_SERVER_IMAGE_LIST"
            , "서버 이미지 목록"
            , "https://ncloud.apigw.ntruss.com"
            , "/vserver/v2/getServerImageList"
            , "GET"
    )
    ;

    @Getter
    private final String apiCode;

    @Getter
    private final String apiName;

    @Getter
    private final String host;

    @Getter
    private final String path;

    @Getter
    private final String method;

    VpcServerImageApiInfos(String apiCode, String apiName, String host, String path, String method) {
        this.apiCode = apiCode;
        this.apiName = apiName;
        this.host = host;
        this.path = path;
        this.method = method;
    }
}
