package cloit.cloud.api.ncpapiservice.v1.api.compute.server.serverimage.domain.request.server;

import cloit.cloud.api.ncpapiservice.v1.comm.domain.NcpApiKeyInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang.StringUtils;

import java.util.List;


@EqualsAndHashCode(callSuper = true)
@Data
public class ServerImageListRequest extends NcpApiKeyInfo {

    private String regionCode;

    private List<String> serverImageNoList;

    private String serverImageName;

    private String serverImageStatusCode;

    private List<String> serverImageTypeCodeList;

    private List<String> hypervisorTypeCodeList;

    private List<String> osTypeCodeList;

    private List<String> platformCategoryCodeList;

    private Integer pageNo;

    private Integer pageSize;

    private String sortedBy;

    private String sortingOrder;

    private String responseFormatType;

    public String getResponseFormatType() {
        return StringUtils.defaultString(responseFormatType, "json");
    }
}
