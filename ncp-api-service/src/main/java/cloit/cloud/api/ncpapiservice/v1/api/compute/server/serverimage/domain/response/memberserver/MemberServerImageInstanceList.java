package cloit.cloud.api.ncpapiservice.v1.api.compute.server.serverimage.domain.response.memberserver;

import lombok.Data;

import java.util.List;

@Data
public class MemberServerImageInstanceList {

    private Integer totalRows;

    private List<MemberServerImageInstance> memberServerImageInstanceList;
}
