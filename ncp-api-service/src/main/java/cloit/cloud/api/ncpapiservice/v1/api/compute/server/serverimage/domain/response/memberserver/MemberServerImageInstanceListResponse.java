package cloit.cloud.api.ncpapiservice.v1.api.compute.server.serverimage.domain.response.memberserver;

import lombok.Data;

@Data
public class MemberServerImageInstanceListResponse {

    private MemberServerImageInstanceList getMemberServerImageInstanceListResponse;
}
