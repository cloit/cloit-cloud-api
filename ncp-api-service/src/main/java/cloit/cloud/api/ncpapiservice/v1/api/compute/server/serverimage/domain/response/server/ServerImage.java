package cloit.cloud.api.ncpapiservice.v1.api.compute.server.serverimage.domain.response.server;

import cloit.cloud.api.ncpapiservice.v1.api.comm.domain.BlockStorageMapping;
import cloit.cloud.api.ncpapiservice.v1.api.comm.domain.CommonCode;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class ServerImage {

    private String serverImageNo;

    private String serverImageName;

    private String serverImageDescription;

    private CommonCode serverImageType;

    private CommonCode hypervisorType;

    private CommonCode cpuArchitectureType;

    private CommonCode osCategoryType;

    private CommonCode osType;

    private String serverImageStatusName;

    private CommonCode serverImageStatus;

    private CommonCode serverImageOperation;

    private String serverImageProductCode;

    private Date createDate;

    private CommonCode shareStatus;

    private List<String> sharedLoginIdList;

    private List<BlockStorageMapping> blockStorageMappingList;
}
