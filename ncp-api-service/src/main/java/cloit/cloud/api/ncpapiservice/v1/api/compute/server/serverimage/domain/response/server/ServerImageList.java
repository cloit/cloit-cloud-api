package cloit.cloud.api.ncpapiservice.v1.api.compute.server.serverimage.domain.response.server;

import lombok.Data;

import java.util.List;

@Data
public class ServerImageList {

    private Integer totalRows;

    private List<ServerImage> serverImageList;
}
