package cloit.cloud.api.ncpapiservice.v1.api.compute.server.serverimage.domain.response.server;

import lombok.Data;

@Data
public class ServerImageListResponse {

    private ServerImageList getServerImageListResponse;
}
