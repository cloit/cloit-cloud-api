package cloit.cloud.api.ncpapiservice.v1.api.compute.server.serverimage.service;

import cloit.cloud.api.core.component.http.domain.HttpRequest;
import cloit.cloud.api.ncpapiservice.v1.api.comm.data.Validation;
import cloit.cloud.api.ncpapiservice.v1.api.compute.server.serverimage.constants.VpcServerImageApiInfos;
import cloit.cloud.api.ncpapiservice.v1.api.compute.server.serverimage.domain.request.memberserver.MemberServerImageInstanceListRequest;
import cloit.cloud.api.ncpapiservice.v1.api.compute.server.serverimage.domain.request.server.ServerImageListRequest;
import cloit.cloud.api.ncpapiservice.v1.api.compute.server.serverimage.domain.response.memberserver.MemberServerImageInstanceListResponse;
import cloit.cloud.api.ncpapiservice.v1.api.compute.server.serverimage.domain.response.server.ServerImageListResponse;
import cloit.cloud.api.ncpapiservice.v1.comm.domain.NcpApiKeyInfo;
import cloit.cloud.api.ncpapiservice.v1.comm.service.NcpHttpService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class VpcServerImageService {

    private final NcpHttpService ncpHttpService;

    /**
     * member server image product code 조회
     * @param memberServerImageInstanceListRequest request 정보
     * @return MemberServerImageInstanceListResponse
     */
    public MemberServerImageInstanceListResponse getMemberServerImageInstanceList(MemberServerImageInstanceListRequest memberServerImageInstanceListRequest){

        Validation.apiKeyCheck(memberServerImageInstanceListRequest);

        final String host = VpcServerImageApiInfos.VPC_MEMBER_SERVER_IMAGE_INSTANCE_LIST.getHost();
        final String path = VpcServerImageApiInfos.VPC_MEMBER_SERVER_IMAGE_INSTANCE_LIST.getPath();

        final HttpRequest httpRequest = HttpRequest.create()
                .get()
                .setHost(host)
                .setPath(path)
                .setQueryParams(memberServerImageInstanceListRequest);

        return ncpHttpService.httpExecute(httpRequest, memberServerImageInstanceListRequest, MemberServerImageInstanceListResponse.class);
    }

    /**
     * 서버 이미지 리스트 조회
     * @param serverImageListRequest request 정보
     * @return ServerImageListResponse
     */
    public ServerImageListResponse getServerImageList(ServerImageListRequest serverImageListRequest){

        Validation.apiKeyCheck(serverImageListRequest);

        final String host = VpcServerImageApiInfos.VPC_SERVER_IMAGE_LIST.getHost();
        final String path = VpcServerImageApiInfos.VPC_SERVER_IMAGE_LIST.getPath();

        final HttpRequest httpRequest = HttpRequest.create()
                .get()
                .setHost(host)
                .setPath(path)
                .setQueryParams(serverImageListRequest);

        return ncpHttpService.httpExecute(httpRequest, serverImageListRequest, ServerImageListResponse.class);

    }
}
