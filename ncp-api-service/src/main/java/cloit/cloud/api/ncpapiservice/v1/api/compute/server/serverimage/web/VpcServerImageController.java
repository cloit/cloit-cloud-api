package cloit.cloud.api.ncpapiservice.v1.api.compute.server.serverimage.web;

import cloit.cloud.api.ncpapiservice.v1.api.comm.data.ResponseData;
import cloit.cloud.api.ncpapiservice.v1.api.compute.server.serverimage.domain.request.memberserver.MemberServerImageInstanceListRequest;
import cloit.cloud.api.ncpapiservice.v1.api.compute.server.serverimage.domain.request.server.ServerImageListRequest;
import cloit.cloud.api.ncpapiservice.v1.api.compute.server.serverimage.domain.response.memberserver.MemberServerImageInstanceListResponse;
import cloit.cloud.api.ncpapiservice.v1.api.compute.server.serverimage.domain.response.server.ServerImageListResponse;
import cloit.cloud.api.ncpapiservice.v1.api.compute.server.serverimage.service.VpcServerImageService;
import cloit.cloud.api.ncpapiservice.v1.comm.domain.NcpApiKeyInfo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.Map;

@RestController
@RequestMapping("/ncp/vpc/server")
@RequiredArgsConstructor
@Slf4j
public class VpcServerImageController {

    private final VpcServerImageService vpcServerImageService;

    /**
     * member server image product code 조회
     * @param memberServerImageInstanceListRequest request 정보
     * @return Mono<Map<String, Object>>
     */
    @RequestMapping("/member/image/instances")
    public Mono<Map<String, Object>> memberServerImageInstances(@RequestBody MemberServerImageInstanceListRequest memberServerImageInstanceListRequest){
        log.debug("##### memberServerImageInstanceListRequest : {}", memberServerImageInstanceListRequest.toString());
        ResponseData responseData = new ResponseData();
        try{
            MemberServerImageInstanceListResponse memberServerImageInstanceListResponse
                    = vpcServerImageService.getMemberServerImageInstanceList(memberServerImageInstanceListRequest);
            responseData.responseSuccess(memberServerImageInstanceListResponse);
        }catch (Exception e){
            log.debug("##### Member Server Image Instances Exception ", e);
            responseData.responseFail(e);
        }
        return Mono.just(responseData.responseResult());
    }

    /**
     * 서버 이미지 목록 조회
     * @param serverImageListRequest request 정보
     * @return Mono<Map<String, Object>>
     */
    @RequestMapping("/images")
    public Mono<Map<String, Object>> serverImages(@RequestBody ServerImageListRequest serverImageListRequest){
        log.debug("##### serverImageListRequest : {}", serverImageListRequest.toString());
        ResponseData responseData = new ResponseData();
        try{
            ServerImageListResponse serverImageListResponse
                    = vpcServerImageService.getServerImageList(serverImageListRequest);
            responseData.responseSuccess(serverImageListResponse);
        }catch (Exception e){
            log.debug("##### Server Images Exception ", e);
            responseData.responseFail(e);
        }
        return Mono.just(responseData.responseResult());
    }
}
