package cloit.cloud.api.ncpapiservice.v1.api.management.insight.constants;

import lombok.Getter;

import java.util.Arrays;

public enum Calculation {

    COUNT("COUNT"),
    SUM("SUM"),
    MAX("MAX"),
    MIN("MIN"),
    AVG("AVG");

    @Getter
    private final String codeId;

    Calculation(String codeId) {
        this.codeId = codeId;
    }

    public static Calculation find(String aggregation){
        return Arrays.stream(values())
                .filter(calculation -> calculation.codeId.equals(aggregation))
                .findAny()
                .orElseThrow(() -> new RuntimeException("Calculation not found"));
    }
}
