package cloit.cloud.api.ncpapiservice.v1.api.management.insight.constants;

import lombok.Getter;

public enum CloudInsightApiInfos {

    QUERY_DATA_MULTIPLE(
            "QUERY_DATA_MULTIPLE"
            , "인스턴스 메트릭 정보 조회"
            , "https://cw.apigw.ntruss.com"
            , "/cw_fea/real/cw/api/data/query/multiple"
            , "POST"
    )
    ;
    @Getter
    private final String apiCode;

    @Getter
    private final String apiName;

    @Getter
    private final String host;

    @Getter
    private final String path;

    @Getter
    private final String method;

    CloudInsightApiInfos(String apiCode, String apiName, String host, String path, String method) {
        this.apiCode = apiCode;
        this.apiName = apiName;
        this.host = host;
        this.path = path;
        this.method = method;
    }
}
