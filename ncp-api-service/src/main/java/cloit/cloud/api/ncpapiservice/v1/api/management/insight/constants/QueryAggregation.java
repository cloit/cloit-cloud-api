package cloit.cloud.api.ncpapiservice.v1.api.management.insight.constants;

public enum QueryAggregation {

    COUNT,
    SUM,
    MAX,
    MIN,
    AVG
}
