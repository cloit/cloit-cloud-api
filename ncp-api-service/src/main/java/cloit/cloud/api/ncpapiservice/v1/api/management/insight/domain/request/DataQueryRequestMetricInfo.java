package cloit.cloud.api.ncpapiservice.v1.api.management.insight.domain.request;

import cloit.cloud.api.ncpapiservice.v1.api.management.insight.constants.Calculation;
import cloit.cloud.api.ncpapiservice.v1.api.management.insight.constants.Interval;
import cloit.cloud.api.ncpapiservice.v1.api.management.insight.constants.QueryAggregation;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class DataQueryRequestMetricInfo {

    private String productName;

    private String prodKey;

    private String metric;

    private Interval interval;

    private Calculation aggregation;

    private QueryAggregation queryAggregation;

    private Map<String, String> dimensions;

    private List<List<Double>> dps;
}
