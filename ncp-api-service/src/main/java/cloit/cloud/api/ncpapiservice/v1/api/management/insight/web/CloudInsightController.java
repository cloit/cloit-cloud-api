package cloit.cloud.api.ncpapiservice.v1.api.management.insight.web;

import cloit.cloud.api.ncpapiservice.v1.api.comm.data.ResponseData;
import cloit.cloud.api.ncpapiservice.v1.api.management.insight.domain.DataQueryParam;
import cloit.cloud.api.ncpapiservice.v1.api.management.insight.domain.response.QueryDataMultipleResponse;
import cloit.cloud.api.ncpapiservice.v1.api.management.insight.service.CloudInsightService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.Map;

@RestController
@RequestMapping("/ncp")
@RequiredArgsConstructor
@Slf4j
public class CloudInsightController {

    private final CloudInsightService cloudInsightService;

    /**
     * kind 별 인스턴스 메트릭 정보 조회
     * @param type 인스턴스 종류
     * @param dataQueryParam 요청 파라미터
     * @return Mono<String>
     */
    @RequestMapping("/query/data/multiple/{type}")
    public Mono<Map<String, Object>> queryDataMultiple(@PathVariable("type") String type,
                                                       @RequestBody DataQueryParam dataQueryParam){
        log.debug("##### type : {}", type);
        log.debug("##### dataQueryParam : {}", dataQueryParam.toString());
        ResponseData responseData = new ResponseData();
        try {
            QueryDataMultipleResponse queryDataMultipleResponse
                    = cloudInsightService.queryDataMultiple(type, dataQueryParam);
            responseData.responseSuccess(queryDataMultipleResponse);

        }catch (Exception e){
            log.debug("##### QueryDataMultiple Exception", e);
            responseData.responseFail(e);
        }
        return Mono.just(responseData.responseResult());
    }
}
