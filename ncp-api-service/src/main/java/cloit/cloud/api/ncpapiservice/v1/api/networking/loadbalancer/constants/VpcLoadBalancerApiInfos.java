package cloit.cloud.api.ncpapiservice.v1.api.networking.loadbalancer.constants;

import lombok.Getter;

public enum VpcLoadBalancerApiInfos {
    VPC_LOAD_BALANCER_INSTANCE_LIST(
            "VPC_LOAD_BALANCER_INSTANCE_LIST"
            , "로드밸런서 인스턴스 리스트 목록"
            , "https://ncloud.apigw.ntruss.com"
            , "/vloadbalancer/v2/getLoadBalancerInstanceList"
            , "GET"
    )
    ;
    @Getter
    private final String apiCode;

    @Getter
    private final String apiName;

    @Getter
    private final String host;

    @Getter
    private final String path;

    @Getter
    private final String method;

    VpcLoadBalancerApiInfos(String apiCode, String apiName, String host, String path, String method) {
        this.apiCode = apiCode;
        this.apiName = apiName;
        this.host = host;
        this.path = path;
        this.method = method;
    }
}
