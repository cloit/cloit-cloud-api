package cloit.cloud.api.ncpapiservice.v1.api.networking.loadbalancer.domain.response;

import cloit.cloud.api.ncpapiservice.v1.api.comm.domain.CommonCode;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class LoadBalancerInstance {

    private String loadBalancerInstanceNo;

    private CommonCode loadBalancerInstanceStatus;

    private CommonCode loadBalancerInstanceOperation;

    private String loadBalancerInstanceStatusName;

    private String loadBalancerDescription;

    private Date createDate;

    private String loadBalancerName;

    private String loadBalancerDomain;

    private List<String> loadBalancerIpList;

    private CommonCode loadBalancerType;

    private CommonCode loadBalancerNetworkType;

    private CommonCode throughputType;

    private Integer idleTimeout;

    private String vpcNo;

    private String regionCode;

    private List<String> subnetNoList;

    private List<LoadBalancerSubnet> loadBalancerSubnetList;

    private List<String> loadBalancerListenerNoList;
}
