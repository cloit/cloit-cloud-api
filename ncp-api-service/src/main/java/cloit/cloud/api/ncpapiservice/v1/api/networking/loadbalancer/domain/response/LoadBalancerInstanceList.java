package cloit.cloud.api.ncpapiservice.v1.api.networking.loadbalancer.domain.response;

import lombok.Data;

import java.util.List;

@Data
public class LoadBalancerInstanceList {

    private Integer totalRows;

    private List<LoadBalancerInstance> loadBalancerInstanceList;
}
