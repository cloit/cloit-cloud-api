package cloit.cloud.api.ncpapiservice.v1.api.networking.loadbalancer.service;

import cloit.cloud.api.core.component.http.domain.HttpRequest;
import cloit.cloud.api.ncpapiservice.v1.api.comm.data.Validation;
import cloit.cloud.api.ncpapiservice.v1.api.networking.loadbalancer.constants.VpcLoadBalancerApiInfos;
import cloit.cloud.api.ncpapiservice.v1.api.networking.loadbalancer.domain.request.LoadBalancerInstanceListRequest;
import cloit.cloud.api.ncpapiservice.v1.api.networking.loadbalancer.domain.response.LoadBalancerInstanceListResponse;
import cloit.cloud.api.ncpapiservice.v1.comm.domain.NcpApiKeyInfo;
import cloit.cloud.api.ncpapiservice.v1.comm.service.NcpHttpService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class VpcLoadBalancerService {

    private final NcpHttpService ncpHttpService;

    /**
     * 로드밸런서 인스턴스 조회
     * @param loadBalancerInstanceListRequest request 정보
     * @return LoadBalancerInstanceListResponse
     */
    public LoadBalancerInstanceListResponse getLoadBalancerInstanceList(LoadBalancerInstanceListRequest loadBalancerInstanceListRequest){

        Validation.apiKeyCheck(loadBalancerInstanceListRequest);

        final String host = VpcLoadBalancerApiInfos.VPC_LOAD_BALANCER_INSTANCE_LIST.getHost();
        final String path = VpcLoadBalancerApiInfos.VPC_LOAD_BALANCER_INSTANCE_LIST.getPath();

        final HttpRequest httpRequest = HttpRequest.create()
                .get()
                .setHost(host)
                .setPath(path)
                .setQueryParams(loadBalancerInstanceListRequest);

        return ncpHttpService.httpExecute(httpRequest, loadBalancerInstanceListRequest, LoadBalancerInstanceListResponse.class);
    }
}
