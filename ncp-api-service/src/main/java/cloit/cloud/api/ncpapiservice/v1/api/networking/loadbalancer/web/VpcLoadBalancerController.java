package cloit.cloud.api.ncpapiservice.v1.api.networking.loadbalancer.web;

import cloit.cloud.api.ncpapiservice.v1.api.comm.data.ResponseData;
import cloit.cloud.api.ncpapiservice.v1.api.networking.loadbalancer.domain.request.LoadBalancerInstanceListRequest;
import cloit.cloud.api.ncpapiservice.v1.api.networking.loadbalancer.domain.response.LoadBalancerInstanceListResponse;
import cloit.cloud.api.ncpapiservice.v1.api.networking.loadbalancer.service.VpcLoadBalancerService;
import cloit.cloud.api.ncpapiservice.v1.comm.domain.NcpApiKeyInfo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.Map;

@RestController
@RequestMapping("/ncp/vpc")
@RequiredArgsConstructor
@Slf4j
public class VpcLoadBalancerController {

    private final VpcLoadBalancerService vpcLoadBalancerService;

    /**
     * vpc 로드밸런서 목록 조회
     * @param loadBalancerInstanceListRequest request 정보
     * @return Mono<Map<String, Object>>
     */
    @RequestMapping("/loadbalancer/instances")
    public Mono<Map<String, Object>> loadBalancerInstances(@RequestBody LoadBalancerInstanceListRequest loadBalancerInstanceListRequest){
        log.debug("##### loadBalancerInstanceListRequest : {}", loadBalancerInstanceListRequest.toString());
        ResponseData responseData = new ResponseData();
        try{
            LoadBalancerInstanceListResponse loadBalancerInstanceListResponse
                    = vpcLoadBalancerService.getLoadBalancerInstanceList(loadBalancerInstanceListRequest);
            responseData.responseSuccess(loadBalancerInstanceListResponse);
        }catch (Exception e){
            log.debug("##### Vpc LoadBalancerInstances Exception", e);
            responseData.responseFail(e);
        }
        return Mono.just(responseData.responseResult());
    }
}
