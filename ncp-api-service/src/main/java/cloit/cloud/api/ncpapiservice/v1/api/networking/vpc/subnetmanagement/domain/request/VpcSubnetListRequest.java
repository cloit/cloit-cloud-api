package cloit.cloud.api.ncpapiservice.v1.api.networking.vpc.subnetmanagement.domain.request;

import cloit.cloud.api.ncpapiservice.v1.comm.domain.NcpApiKeyInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang.StringUtils;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class VpcSubnetListRequest extends NcpApiKeyInfo {

    private String regionCOde;

    private List<String> subnetNoList;

    private String subnetName;

    private String subnet;

    private String subnetTypeCode;

    private String usageTypeCode;

    private String networkAclNo;

    private Integer pageNo;

    private Integer pageSize;

    private String subnetStatusCode;

    private String vpcNo;

    private String zoneCode;

    private String responseFormatType;

    public String getResponseFormatType() {
        return StringUtils.defaultString(responseFormatType, "json");
    }
}
