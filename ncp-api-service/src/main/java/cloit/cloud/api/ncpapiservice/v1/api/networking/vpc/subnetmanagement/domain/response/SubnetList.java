package cloit.cloud.api.ncpapiservice.v1.api.networking.vpc.subnetmanagement.domain.response;

import lombok.Data;

import java.util.List;

@Data
public class SubnetList {

    private Integer totalRows;

    private List<Subnet> subnetList;
}
