package cloit.cloud.api.ncpapiservice.v1.api.networking.vpc.subnetmanagement.domain.response;

import lombok.Data;

@Data
public class VpcSubnetListResponse {

    private SubnetList getSubnetListResponse;
}
