package cloit.cloud.api.ncpapiservice.v1.api.networking.vpc.subnetmanagement.service;

import cloit.cloud.api.core.component.http.domain.HttpRequest;
import cloit.cloud.api.ncpapiservice.v1.api.comm.data.Validation;
import cloit.cloud.api.ncpapiservice.v1.api.networking.vpc.subnetmanagement.constants.VpcSubnetManagementApiInfos;
import cloit.cloud.api.ncpapiservice.v1.api.networking.vpc.subnetmanagement.domain.request.VpcSubnetListRequest;
import cloit.cloud.api.ncpapiservice.v1.api.networking.vpc.subnetmanagement.domain.response.VpcSubnetListResponse;
import cloit.cloud.api.ncpapiservice.v1.comm.domain.NcpApiKeyInfo;
import cloit.cloud.api.ncpapiservice.v1.comm.service.NcpHttpService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class VpcSubnetManagementService {

    private final NcpHttpService ncpHttpService;

    /**
     * VPC subnet 목록 조회
     * @param vpcSubnetListRequest request 정보
     * @return VpcSubnetListResponse
     */
    public VpcSubnetListResponse getSubnetList(VpcSubnetListRequest vpcSubnetListRequest){

        Validation.apiKeyCheck(vpcSubnetListRequest);

        final String host = VpcSubnetManagementApiInfos.VPC_SUBNET_LIST.getHost();
        final String path = VpcSubnetManagementApiInfos.VPC_SUBNET_LIST.getPath();

        final HttpRequest httpRequest = HttpRequest.create()
                .get()
                .setHost(host)
                .setPath(path)
                .setQueryParams(vpcSubnetListRequest);

        return ncpHttpService.httpExecute(httpRequest, vpcSubnetListRequest, VpcSubnetListResponse.class);

    }
}
