package cloit.cloud.api.ncpapiservice.v1.api.networking.vpc.subnetmanagement.web;

import cloit.cloud.api.ncpapiservice.v1.api.comm.data.ResponseData;
import cloit.cloud.api.ncpapiservice.v1.api.networking.vpc.subnetmanagement.domain.request.VpcSubnetListRequest;
import cloit.cloud.api.ncpapiservice.v1.api.networking.vpc.subnetmanagement.domain.response.VpcSubnetListResponse;
import cloit.cloud.api.ncpapiservice.v1.api.networking.vpc.subnetmanagement.service.VpcSubnetManagementService;
import cloit.cloud.api.ncpapiservice.v1.comm.domain.NcpApiKeyInfo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.Map;

@RestController
@RequestMapping("/ncp/vpc")
@RequiredArgsConstructor
@Slf4j
public class VpcSubnetManagementController {

    private final VpcSubnetManagementService vpcSubnetManagementService;

    /**
     * VPC subnet 조회
     * @param vpcSubnetListRequest request 정보
     * @return Mono<Map<String, Object>>
     */
    @RequestMapping("/subnets")
    public Mono<Map<String, Object>> subnets(@RequestBody VpcSubnetListRequest vpcSubnetListRequest){
        log.debug("###### vpcSubnetListRequest : {}", vpcSubnetListRequest.toString());
        final ResponseData responseData = new ResponseData();
        try{
            final VpcSubnetListResponse vpcSubnetListResponse
                    = vpcSubnetManagementService.getSubnetList(vpcSubnetListRequest);
            responseData.responseSuccess(vpcSubnetListResponse);
        }catch (Exception e){
            log.debug("##### VPC Subnets Exception ", e);
            responseData.responseFail(e);
        }
        return Mono.just(responseData.responseResult());
    }
}
