package cloit.cloud.api.ncpapiservice.v1.api.networking.vpc.vpcmanagement.domain.request;

import cloit.cloud.api.ncpapiservice.v1.comm.domain.NcpApiKeyInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang.StringUtils;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class VpcListRequest extends NcpApiKeyInfo {

    private String regionCode;

    private String vpcStatusCode;

    private String vpcName;

    private List<String> vpcNoList;

    private String responseFormatType;

    public String getResponseFormatType() {
        return StringUtils.defaultString(responseFormatType, "json");
    }
}
