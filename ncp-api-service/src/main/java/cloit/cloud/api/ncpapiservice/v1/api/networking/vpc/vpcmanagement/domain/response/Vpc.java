package cloit.cloud.api.ncpapiservice.v1.api.networking.vpc.vpcmanagement.domain.response;

import cloit.cloud.api.ncpapiservice.v1.api.comm.domain.CommonCode;
import lombok.Data;

import java.util.Date;

@Data
public class Vpc {

    private String vpcNo;

    private String vpcName;

    private String ipv4CidrBlock;

    private CommonCode vpcStatus;

    private String regionCode;

    private Date createDate;

}
