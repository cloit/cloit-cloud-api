package cloit.cloud.api.ncpapiservice.v1.api.networking.vpc.vpcmanagement.domain.response;

import lombok.Data;

import java.util.List;

@Data
public class VpcList {

    private Integer totalRows;

    private List<Vpc> vpcList;
}
