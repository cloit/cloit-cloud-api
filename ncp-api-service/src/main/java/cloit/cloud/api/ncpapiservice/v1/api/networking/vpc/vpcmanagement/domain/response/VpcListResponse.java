package cloit.cloud.api.ncpapiservice.v1.api.networking.vpc.vpcmanagement.domain.response;

import lombok.Data;

@Data
public class VpcListResponse {

    private VpcList getVpcListResponse;
}
