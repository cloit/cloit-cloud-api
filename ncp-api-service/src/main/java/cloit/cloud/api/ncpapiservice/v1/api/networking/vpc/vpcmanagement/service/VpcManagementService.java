package cloit.cloud.api.ncpapiservice.v1.api.networking.vpc.vpcmanagement.service;

import cloit.cloud.api.core.component.http.domain.HttpRequest;
import cloit.cloud.api.ncpapiservice.v1.api.comm.data.Validation;
import cloit.cloud.api.ncpapiservice.v1.api.networking.vpc.vpcmanagement.constants.VpcManagementApiInfos;
import cloit.cloud.api.ncpapiservice.v1.api.networking.vpc.vpcmanagement.domain.request.VpcListRequest;
import cloit.cloud.api.ncpapiservice.v1.api.networking.vpc.vpcmanagement.domain.response.VpcListResponse;
import cloit.cloud.api.ncpapiservice.v1.comm.domain.NcpApiKeyInfo;
import cloit.cloud.api.ncpapiservice.v1.comm.service.NcpHttpService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class VpcManagementService {

    private final NcpHttpService ncpHttpService;

    /**
     * VPC 목록 조회
     * @param vpcListRequest request 정보
     * @return VpcListResponse
     */
    public VpcListResponse getVpcList(VpcListRequest vpcListRequest){

        Validation.apiKeyCheck(vpcListRequest);

        final String host = VpcManagementApiInfos.VPC_LIST.getHost();
        final String path = VpcManagementApiInfos.VPC_LIST.getPath();

        final HttpRequest httpRequest = HttpRequest.create()
                .get()
                .setHost(host)
                .setPath(path)
                .setQueryParams(vpcListRequest);

        return ncpHttpService.httpExecute(httpRequest, vpcListRequest, VpcListResponse.class);
    }
}
