package cloit.cloud.api.ncpapiservice.v1.api.networking.vpc.vpcmanagement.web;

import cloit.cloud.api.ncpapiservice.v1.api.comm.data.ResponseData;
import cloit.cloud.api.ncpapiservice.v1.api.networking.vpc.vpcmanagement.domain.request.VpcListRequest;
import cloit.cloud.api.ncpapiservice.v1.api.networking.vpc.vpcmanagement.domain.response.VpcListResponse;
import cloit.cloud.api.ncpapiservice.v1.api.networking.vpc.vpcmanagement.service.VpcManagementService;
import cloit.cloud.api.ncpapiservice.v1.comm.domain.NcpApiKeyInfo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.Map;

@RestController
@RequestMapping("/ncp/vpc")
@RequiredArgsConstructor
@Slf4j
public class VpcManagementController {

    private final VpcManagementService vpcManagementService;

    /**
     * Vpc 목록 조회
     * @param vpcListRequest request 정보
     * @return Mono<Map<String, Object>>
     */
    @RequestMapping("/list")
    public Mono<Map<String, Object>> vpcList(@RequestBody VpcListRequest vpcListRequest){
        log.debug("##### vpcListRequest : {}", vpcListRequest);
        final ResponseData responseData = new ResponseData();
        try{
            final VpcListResponse vpcListResponse = vpcManagementService.getVpcList(vpcListRequest);
            responseData.responseSuccess(vpcListResponse);
        }catch (Exception e){
            log.debug("##### Vpc List Exception ", e);
            responseData.responseFail(e);
        }
        return Mono.just(responseData.responseResult());
    }
}
