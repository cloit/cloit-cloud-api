package cloit.cloud.api.ncpapiservice.v1.api.storage.nas.domain.request;

import lombok.Data;
import org.apache.commons.lang.StringUtils;

import java.util.List;

@Data
public class NasVolumeInstanceListRequest {

    private String regionCode;

    private String zoneCode;

    private List<String> nasVolumeInstanceNoList;

    private String volumeName;

    private String volumeAllotmentProtocolTypeCode;

    private Boolean isEventConfiguration;

    private Boolean isSnapshotConfiguration;

    private Integer pageNo;

    private Integer pageSize;

    private String sortedBy;

    private String sortingOrder;

    private String responseFormatType;

    public String getResponseFormatType() {
        return StringUtils.defaultString(responseFormatType, "json");
    }
}
