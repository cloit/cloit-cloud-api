package cloit.cloud.api.ncpapiservice.v1.api.storage.nas.domain.response;

import lombok.Data;

import java.util.List;

@Data
public class ClassicNasVolumeInstanceList {

    private Integer totalRows;

    private List<ClassicNasVolumeInstance> nasVolumeInstanceList;
}
