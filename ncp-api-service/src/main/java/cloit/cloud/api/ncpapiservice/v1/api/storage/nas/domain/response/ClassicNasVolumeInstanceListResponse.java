package cloit.cloud.api.ncpapiservice.v1.api.storage.nas.domain.response;

import lombok.Data;

@Data
public class ClassicNasVolumeInstanceListResponse {

    private ClassicNasVolumeInstanceList getNasVolumeInstanceListResponse;
}
