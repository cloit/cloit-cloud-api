package cloit.cloud.api.ncpapiservice.v1.api.storage.nas.domain.response;

import cloit.cloud.api.ncpapiservice.v1.api.comm.domain.CommonCode;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class NasVolumeInstance {

    private String nasVolumeInstanceNo;

    private CommonCode nasVolumeInstanceStatus;

    private CommonCode nasVolumeInstanceOperation;

    private String nasVolumeInstanceStatusName;

    private Date createDate;

    private String nasVolumeDescription;

    private String mountInformation;

    private CommonCode volumeAllotmentProtocolType;

    private String volumeName;

    private Long volumeTotalSize;

    private Long volumeSize;

    private Float snapshotVolumeConfigurationRatio;

    private CommonCode snapshotVolumeConfigPeriodType;

    private CommonCode snapshotVolumeConfigDayOfWeekType;

    private Integer snapshotVolumeConfigTime;

    private Long snapshotVolumeSize;

    private Boolean isSnapshotConfiguration;

    private Boolean isEventConfiguration;

    private String regionCode;

    private String zoneCode;

    private List<String> nasVolumeServerInstanceNoList;

    private Boolean isEncryptedVolume;

    private List<NasVolumeInstanceCustomIp> nasVolumeInstanceCustomIpList;

    private Boolean isReturnProtection;
}
