package cloit.cloud.api.ncpapiservice.v1.api.storage.nas.service;

import cloit.cloud.api.core.component.http.domain.HttpRequest;
import cloit.cloud.api.ncpapiservice.v1.api.comm.data.Validation;
import cloit.cloud.api.ncpapiservice.v1.api.storage.nas.constants.VpcNasApiInfos;
import cloit.cloud.api.ncpapiservice.v1.api.storage.nas.domain.request.NasVolumeInstanceListRequest;
import cloit.cloud.api.ncpapiservice.v1.api.storage.nas.domain.response.NasVolumeInstanceListResponse;
import cloit.cloud.api.ncpapiservice.v1.comm.domain.NcpApiKeyInfo;
import cloit.cloud.api.ncpapiservice.v1.comm.service.NcpHttpService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class VpcNasService {

    private final NcpHttpService ncpHttpService;

    /**
     * NAS 볼륨 인스턴스 조회
     * @param ncpApiKeyInfo NCP api key 정보
     * @return NasVolumeInstanceListResponse
     */
    public NasVolumeInstanceListResponse getNasVolumeInstanceList(NcpApiKeyInfo ncpApiKeyInfo){

        Validation.apiKeyCheck(ncpApiKeyInfo);

        final String host = VpcNasApiInfos.VPC_NAS_VOLUME_INSTANCE_LIST.getHost();
        final String path = VpcNasApiInfos.VPC_NAS_VOLUME_INSTANCE_LIST.getPath();
        final NasVolumeInstanceListRequest nasVolumeInstanceListRequest
                = new NasVolumeInstanceListRequest();

        HttpRequest httpRequest = HttpRequest.create()
                .get()
                .setHost(host)
                .setPath(path)
                .setQueryParams(nasVolumeInstanceListRequest);

        return ncpHttpService.httpExecute(httpRequest, ncpApiKeyInfo, NasVolumeInstanceListResponse.class);
    }

}
