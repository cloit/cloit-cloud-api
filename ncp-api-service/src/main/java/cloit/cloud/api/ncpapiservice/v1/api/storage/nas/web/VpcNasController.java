package cloit.cloud.api.ncpapiservice.v1.api.storage.nas.web;

import cloit.cloud.api.ncpapiservice.v1.api.comm.data.ResponseData;
import cloit.cloud.api.ncpapiservice.v1.api.storage.nas.domain.response.NasVolumeInstanceListResponse;
import cloit.cloud.api.ncpapiservice.v1.api.storage.nas.service.VpcNasService;
import cloit.cloud.api.ncpapiservice.v1.comm.domain.NcpApiKeyInfo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.Map;

@RestController
@RequestMapping("/ncp/vpc")
@RequiredArgsConstructor
@Slf4j
public class VpcNasController {

    private final VpcNasService vpcNasService;

    /**
     * NAS 볼륨 인스턴스 조회
     * @param ncpApiKeyInfo NCP api key 정보
     * @return Mono<Map<String, Object>>
     */
    @RequestMapping("/nas/instances")
    public Mono<Map<String, Object>> nasInstances(@RequestBody NcpApiKeyInfo ncpApiKeyInfo){
        log.debug("##### ncpApiKeyInfo : {}", ncpApiKeyInfo.toString());
        ResponseData responseData = new ResponseData();
        try{
            NasVolumeInstanceListResponse nasVolumeInstanceListResponse
                    = vpcNasService.getNasVolumeInstanceList(ncpApiKeyInfo);
            responseData.responseSuccess(nasVolumeInstanceListResponse);
        }catch (Exception e){
            log.debug("##### Vpc NasInstances Exception", e);
            responseData.responseFail(e);
        }
        return Mono.just(responseData.responseResult());
    }
}
