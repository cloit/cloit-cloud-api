package cloit.cloud.api.ncpapiservice.v1.api.storage.object.domain;

import lombok.Data;
import org.apache.commons.lang.StringUtils;

@Data
public class ObjectStorageAuthInfo {

    /**
     * NCP API Access Key
     */
    private String accessKey;

    /**
     * NCP API Secret Key
     */
    private String secretKey;

    private String region;

    public String getRegion(){
        return StringUtils.defaultString(this.region, "kr-standard");
    }
}
