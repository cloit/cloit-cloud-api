package cloit.cloud.api.ncpapiservice.v1.api.storage.object.domain.response;

import lombok.Data;

import java.util.List;

@Data
public class Buckets {

    private List<Bucket> bucket;
}
