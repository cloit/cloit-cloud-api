package cloit.cloud.api.ncpapiservice.v1.api.storage.object.domain.response;

import lombok.Data;

@Data
public class ListBuckets {

    private Owner owner;

    private Buckets buckets;
}
