package cloit.cloud.api.ncpapiservice.v1.api.storage.object.service;

import cloit.cloud.api.core.component.http.client.HttpClient;
import cloit.cloud.api.core.component.http.domain.HttpRequest;
import cloit.cloud.api.core.component.http.domain.HttpResponse;
import cloit.cloud.api.core.component.http.gateway.Gateway;
import cloit.cloud.api.ncpapiservice.v1.api.storage.object.domain.ObjectStorageAuthInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.Header;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class ObjectStorageGateway implements Gateway<ObjectStorageAuthInfo> {

    private final HttpClient httpClient;

    public ObjectStorageGateway(@Qualifier("poolingHttpClient") HttpClient httpClient) {
        this.httpClient = httpClient;
    }

    @Override
    public void preExecute(HttpRequest httpRequest, ObjectStorageAuthInfo objectStorageAuthInfo) {
        String accessKey    = objectStorageAuthInfo.getAccessKey();
        String secretKey    = objectStorageAuthInfo.getSecretKey();
        String region       = objectStorageAuthInfo.getRegion();
        try{
            AwsRequestSigningService.authorization(httpRequest, region, accessKey, secretKey);
        }catch (Exception e){
            log.debug("##### Object Storage Aws Signature Setting Exception ", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public HttpResponse execute(HttpRequest httpRequest) {
        return httpClient.execute(httpRequest);
    }

    @Override
    public void postExecute(HttpResponse httpResponse) {

    }
}
