package cloit.cloud.api.ncpapiservice.v1.api.storage.object.service;

import cloit.cloud.api.core.component.http.domain.HttpRequest;
import cloit.cloud.api.core.component.http.domain.HttpResponse;
import cloit.cloud.api.core.component.http.gateway.Gateway;
import cloit.cloud.api.core.utils.ObjectMapperUtils;
import cloit.cloud.api.core.utils.XmlUtils;
import cloit.cloud.api.ncpapiservice.v1.api.storage.object.domain.ObjectStorageAuthInfo;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.*;

@Service
@Slf4j
public class ObjectStorageHttpService {

    private final Gateway<ObjectStorageAuthInfo> gateway;

    public ObjectStorageHttpService(@Qualifier("objectStorageGateway") Gateway<ObjectStorageAuthInfo> gateway) {
        this.gateway = gateway;
    }

    public <T> T httpExecute(HttpRequest httpRequest, ObjectStorageAuthInfo objectStorageAuthInfo, Type type) throws JsonProcessingException {
        final HttpResponse httpResponse = gateway.transmit(httpRequest, objectStorageAuthInfo);
        final int statusCode = httpResponse.getStatusCode();
        final String headerString = ArrayUtils.toString(httpResponse.getHeaders());
        final String responseBody = httpResponse.getEntityAsJson();

        log.debug("##### statusCode : {}", statusCode);
        log.debug("##### headerString : {}", headerString);
        log.debug("##### responseBody : {}", responseBody);

        if( statusCode != 200 ){
            throw new RuntimeException("##### API_RESPONSE_FAIL");
        }

        Map<String, Object> convertMap = firstCharacterLowerCase(responseBody);

        return ObjectMapperUtils.mapToObject(convertMap, type);

    }

    /**
     * Xml 데이터 key 값의 첫번째 문자열을 소문자로 치환
     * @param responseBody xml string 데이터
     * @return Map<String, Object>
     */
    private Map<String, Object> firstCharacterLowerCase(String responseBody) throws JsonProcessingException{
        Map<String, Object> responseMap = XmlUtils.fromXml(responseBody, new TypeReference<Map<String, Object>>(){}.getType());
        return responseMap.entrySet().stream()
                .collect(HashMap::new, (map, entry) -> {
                    mapChildConverter(map, entry.getKey(), entry.getValue());
                }, HashMap::putAll);
    }

    /**
     * map 하위 object 변환
     * @param map map
     * @param orgKey 기존키
     * @param value 신규키
     */
    private void mapChildConverter(Map<String, Object> map, String orgKey, Object value){
        Object resultValue = value;
        String newKey = Character.toLowerCase(orgKey.charAt(0)) + orgKey.substring(1);
        if( orgKey.equals("ID") ){
            newKey = "id";
        }

        if( value instanceof Map ){
            Map<String, Object> childMap = new LinkedHashMap<>();
            for(Object k : ((Map<?, ?>) value).keySet()){
                Object v = ((Map<?, ?>) value).get(k);
                mapChildConverter(childMap, (String) k, v);
            }
            resultValue = childMap;
        }

        if( value instanceof List ){
            List<Map<String, Object>> childList = new ArrayList<>();
            for( Object o : new ArrayList<>((Collection<?>) value) ){
                Map<String, Object> childMap = new LinkedHashMap<>();
                if( o instanceof Map ){
                    for(Object k : ((Map<?, ?>) o).keySet()){
                        Object v = ((Map<?, ?>) o).get(k);
                        mapChildConverter(childMap, (String) k, v);
                    }
                }
                childList.add(childMap);
            }
            resultValue = childList;
        }

        map.put(newKey, resultValue);
    }
}
