package cloit.cloud.api.ncpapiservice.v1.api.storage.object.service;

import cloit.cloud.api.core.component.http.domain.HttpRequest;
import cloit.cloud.api.ncpapiservice.v1.api.storage.object.constants.ObjectStorageApiInfos;
import cloit.cloud.api.ncpapiservice.v1.api.storage.object.constants.RegionDomain;
import cloit.cloud.api.ncpapiservice.v1.api.storage.object.domain.ObjectStorageAuthInfo;
import cloit.cloud.api.ncpapiservice.v1.api.storage.object.domain.response.ListBuckets;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class ObjectStorageService {

    private final ObjectStorageHttpService objectStorageHttpService;

    /**
     * ObjectStorage 버킷 목록 조회
     * @param objectStorageAuthInfo ObjectStorage 인증 정보
     * @return ListBuckets
     */
    public ListBuckets listBuckets(ObjectStorageAuthInfo objectStorageAuthInfo) throws JsonProcessingException {

        validationCheck(objectStorageAuthInfo);

        final String region = objectStorageAuthInfo.getRegion();
        final String host = RegionDomain.find(region).getDomain();
        final String path = ObjectStorageApiInfos.LIST_BUCKETS.getPath();
        HttpRequest httpRequest = HttpRequest.create()
                .get()
                .setHost(host)
                .setPath(path);

        return objectStorageHttpService.httpExecute(httpRequest, objectStorageAuthInfo, ListBuckets.class);

    }

    /**
     * 유효성 검사
     * @param objectStorageAuthInfo ObjectStorage 인증 정보
     */
    public void validationCheck(ObjectStorageAuthInfo objectStorageAuthInfo){
        final String accessKey = objectStorageAuthInfo.getAccessKey();
        final String secretKey = objectStorageAuthInfo.getSecretKey();

        if(StringUtils.isEmpty(accessKey)){
            throw new RuntimeException("Access Key is empty");
        }
        if(StringUtils.isEmpty(secretKey)){
            throw new RuntimeException("Secret Key is empty");
        }
    }
}
