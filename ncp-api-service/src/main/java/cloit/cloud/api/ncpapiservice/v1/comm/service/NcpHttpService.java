package cloit.cloud.api.ncpapiservice.v1.comm.service;

import cloit.cloud.api.core.component.http.domain.HttpRequest;
import cloit.cloud.api.core.component.http.domain.HttpResponse;
import cloit.cloud.api.core.component.http.gateway.Gateway;
import cloit.cloud.api.core.utils.GsonUtils;
import cloit.cloud.api.ncpapiservice.v1.comm.domain.NcpApiKeyInfo;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;

@Service
@Slf4j
public class NcpHttpService {

    private final Gateway<NcpApiKeyInfo> gateway;

    public NcpHttpService(@Qualifier("ncpGateway") Gateway<NcpApiKeyInfo> gateway) {
        this.gateway = gateway;
    }

    public <T> T httpExecute(HttpRequest httpRequest, NcpApiKeyInfo ncpApiKeyInfo, Type type){
        final HttpResponse httpResponse = gateway.transmit(httpRequest, ncpApiKeyInfo);
        final int statusCode = httpResponse.getStatusCode();
        final String headerString = ArrayUtils.toString(httpResponse.getHeaders());
        final String responseBody = httpResponse.getEntityAsJson();

        log.debug("##### statusCode : {}", statusCode);
        log.debug("##### headerString : {}", headerString);
        log.debug("##### responseBody : {}", responseBody);

        if( statusCode != 200 ){
            final JsonElement jsonElement = JsonParser.parseString(responseBody);
            final JsonObject jsonObject = jsonElement.getAsJsonObject();

            String errorMessage = StringUtils.EMPTY;
            if( jsonObject.has("responseError") ){
                errorMessage = jsonObject.getAsJsonObject("responseError")
                        .get("returnMessage").getAsString();
            }else if( jsonObject.has("error") ){
                errorMessage = jsonObject.getAsJsonObject("error")
                        .get("details").getAsString();
            }

            throw new RuntimeException(errorMessage);
        }

        return GsonUtils.fromJson(responseBody, type);
    }
}
