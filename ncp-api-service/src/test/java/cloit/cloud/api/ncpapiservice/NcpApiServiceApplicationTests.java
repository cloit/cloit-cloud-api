package cloit.cloud.api.ncpapiservice;

import cloit.cloud.api.ncpapiservice.config.NcpApiServiceApplication;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = NcpApiServiceApplication.class)
class NcpApiServiceApplicationTests {

    @Test
    void contextLoads() {
        System.out.println("##### TEST");
    }

}
