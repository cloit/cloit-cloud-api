package cloit.cloud.api.ncpapiservice.api.computer;

import cloit.cloud.api.core.utils.DateUtils;
import cloit.cloud.api.core.utils.GsonUtils;
import cloit.cloud.api.ncpapiservice.config.NcpApiServiceApplication;
import cloit.cloud.api.ncpapiservice.v1.api.networking.loadbalancer.domain.response.LoadBalancerInstanceListResponse;
import cloit.cloud.api.ncpapiservice.v1.api.networking.loadbalancer.service.VpcLoadBalancerService;
import cloit.cloud.api.ncpapiservice.v1.api.compute.server.server.service.VpcServerService;
import cloit.cloud.api.ncpapiservice.v1.comm.domain.NcpApiKeyInfo;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.text.DecimalFormat;
import java.util.Date;

@SpringBootTest(classes = NcpApiServiceApplication.class)
@Slf4j
public class InstanceApiTest {

    @Autowired
    private VpcServerService vpcServerService;

    @Autowired
    private VpcLoadBalancerService vpcLoadBalancerService;

    private final String accessKey = "EE40BFFC9B8CE2ED3DCD";
    private final String secretKey = "6A8601DF0CEF94DF3FF604FF7E77990592DFD762";

//    @Test
//    void getServerInstanceList(){
//        NcpApiKeyInfo ncpApiKeyInfo = new NcpApiKeyInfo();
//        ncpApiKeyInfo.setAccessKey(accessKey);
//        ncpApiKeyInfo.setSecretKey(secretKey);
//
//        vpcServerService.getServerInstanceList(ncpApiKeyInfo);
//    }
//
//    @Test
//    void getLoadBalancerInstanceList(){
//        NcpApiKeyInfo ncpApiKeyInfo = new NcpApiKeyInfo();
//        ncpApiKeyInfo.setAccessKey(accessKey);
//        ncpApiKeyInfo.setSecretKey(secretKey);
//
//        LoadBalancerInstanceListResponse loadBalancerInstanceListResponse
//                = vpcLoadBalancerService.getLoadBalancerInstanceList(ncpApiKeyInfo);
//        log.debug("##### loadBalancerInstanceListResponse : {}", GsonUtils.toJson(loadBalancerInstanceListResponse));
//    }
//
//    @Test
//    void numberFormat(){
//        String numberStr = "1.7083548E12";
//        String numberStr2 = "1.7084412E12";
//        DecimalFormat decimalFormat = new DecimalFormat("######.#####");
//        long value = Long.parseLong(decimalFormat.format(Double.valueOf(numberStr)));
//        long value2 = Long.parseLong(decimalFormat.format(Double.valueOf(numberStr2)));
//
//        log.debug("##### date2 : {}", DateUtils.getDateToString(new Date(value), "yyyyMMdd HH:mm:ss"));
//        log.debug("##### date2 : {}", DateUtils.getDateToString(new Date(value2), "yyyyMMdd HH:mm:ss"));
//
//    }

}
