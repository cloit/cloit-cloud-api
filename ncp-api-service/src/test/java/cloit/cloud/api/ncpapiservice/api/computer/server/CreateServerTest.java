package cloit.cloud.api.ncpapiservice.api.computer.server;

import cloit.cloud.api.core.utils.GsonUtils;
import cloit.cloud.api.ncpapiservice.config.NcpApiServiceApplication;
import cloit.cloud.api.ncpapiservice.v1.api.comm.data.RequestDataConverter;
import cloit.cloud.api.ncpapiservice.v1.api.compute.server.server.domain.request.server.CreateServerInstanceRequest;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@SpringBootTest(classes = NcpApiServiceApplication.class)
@Slf4j
public class CreateServerTest {

    @Test
    void NetworkInterfaceObjectTest(){
        try{
//            String jsonStr = "{    \"accessKey\": \"DACA725DA842F5742A69\",    \"secretKey\": \"5189505EB580B685A57F3B62314CB3056B7FD40F\",    \"regionCode\": \"KR\",    \"serverImageProductCode\": \"SW.VSVR.OS.LNX64.UBNTU.SVR2004.B050\",    \"vpcNo\": \"25726\",    \"subnetNo\": \"91767\",    \"serverCreateCount\": 1,    \"networkInterfaceList\": [        {            \"networkInterfaceOrder\": 0        }    ],    \"serverName\": \"mhpark-auto-create\",    \"associateWithPublicIp\": true,    \"isProtectServerTermination\": false}";
//            String jsonStr = "{    \"accessKey\": \"DACA725DA842F5742A69\",    \"secretKey\": \"5189505EB580B685A57F3B62314CB3056B7FD40F\",    \"regionCode\": \"KR\",    \"serverImageProductCode\": \"SW.VSVR.OS.LNX64.UBNTU.SVR2004.B050\",    \"vpcNo\": \"25726\",    \"subnetNo\": \"91767\",    \"serverCreateCount\": 1,    \"networkInterfaceList\": [        {            \"networkInterfaceOrder\": 0,\t\t\t\"accessGroupNoList\": [123]        }    ],    \"serverName\": \"mhpark-auto-create\",    \"associateWithPublicIp\": true,    \"isProtectServerTermination\": false,\t\"loginKeyName\" : \"test-mhpark\"}";
            String jsonStr = "{    \"accessKey\": \"DACA725DA842F5742A69\",    \"secretKey\": \"5189505EB580B685A57F3B62314CB3056B7FD40F\",    \"regionCode\": \"KR\",    \"serverImageProductCode\": \"SW.VSVR.OS.LNX64.UBNTU.SVR2004.B050\",    \"vpcNo\": \"25726\",    \"subnetNo\": \"91767\",    \"serverCreateCount\": 1,    \"networkInterfaceList\": [        {            \"networkInterfaceOrder\": 0,\t\t\t\"accessControlGroupNoList\" : [\"123\", \"234\"]        },\t\t{\t\t\t\"accessControlGroupNoList\" : [\"123\", \"234\", \"345\"]        }    ],\t\"networkInterfaceList2\" : [\"aaaa\", \"bbbbb\"],    \"serverName\": \"mhpark-auto-create\",    \"associateWithPublicIp\": true,    \"isProtectServerTermination\": false}";
            CreateServerInstanceRequest createServerInstanceRequest = GsonUtils.fromJson(jsonStr, CreateServerInstanceRequest.class);
            log.debug("##### createServerInstanceRequest : {}", GsonUtils.toJson(createServerInstanceRequest));


            Object map = RequestDataConverter.requestDataConverter(createServerInstanceRequest);
            log.debug("##### map : {}", GsonUtils.toJson(map));


        }catch (Exception e){
            log.debug("##### Exception ", e);
        }
    }

    @Test
    void MapDataMapTest(){
        try{
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("A", 123);
            map.put("B", 234);

        }catch (Exception e){
            log.debug("##### Exception ", e);
        }
    }
}
