package cloit.cloud.api.ncpapiservice.api.kubernetes;

import cloit.cloud.api.core.component.http.domain.HttpRequest;
import cloit.cloud.api.core.utils.GsonUtils;
import cloit.cloud.api.ncpapiservice.config.NcpApiServiceApplication;
import cloit.cloud.api.ncpapiservice.v1.comm.domain.NcpApiKeyInfo;
import cloit.cloud.api.ncpapiservice.v1.comm.service.NcpHttpService;
import com.fasterxml.jackson.core.type.TypeReference;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Map;

@SpringBootTest(classes = NcpApiServiceApplication.class)
@Slf4j
public class KubernetesApiTest {

    @Autowired
    private NcpHttpService ncpHttpService;

    private final String accessKey = "EE40BFFC9B8CE2ED3DCD";
    private final String secretKey = "6A8601DF0CEF94DF3FF604FF7E77990592DFD762";

    @Test
    void kubernetesApi(){
        try{

            NcpApiKeyInfo ncpApiKeyInfo = new NcpApiKeyInfo();
            ncpApiKeyInfo.setAccessKey(accessKey);
            ncpApiKeyInfo.setSecretKey(secretKey);

            HttpRequest httpRequest = HttpRequest.create()
                    .setHost("https://nks.apigw.ntruss.com")
                    .setPath("/vnks/v2/clusters")
                    .get();

            Map<String, Object> resMap
                    = ncpHttpService.httpExecute(httpRequest, ncpApiKeyInfo, new TypeReference<Map<String, Object>>(){}.getType());

            log.debug("##### resMap : {}", GsonUtils.toJson(resMap));
        }catch (Exception e){
            log.debug("##### Exception ", e);
        }
    }
}
