package cloit.cloud.api.ncpapiservice.api.storage;

import cloit.cloud.api.core.component.http.domain.HttpRequest;
import cloit.cloud.api.core.utils.GsonUtils;
import cloit.cloud.api.ncpapiservice.config.NcpApiServiceApplication;
import cloit.cloud.api.ncpapiservice.v1.api.storage.object.constants.ObjectStorageApiInfos;
import cloit.cloud.api.ncpapiservice.v1.api.storage.object.domain.ObjectStorageAuthInfo;
import cloit.cloud.api.ncpapiservice.v1.api.storage.object.domain.response.ListBuckets;
import cloit.cloud.api.ncpapiservice.v1.api.storage.object.domain.response.Owner;
import cloit.cloud.api.ncpapiservice.v1.api.storage.object.service.ObjectStorageHttpService;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.Bucket;
import com.fasterxml.jackson.core.type.TypeReference;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Map;


@SpringBootTest(classes = NcpApiServiceApplication.class)
@Slf4j
public class StorageApiTest {

    @Autowired
    private ObjectStorageHttpService objectStorageHttpService;

    private final String accessKey = "DACA725DA842F5742A69";

    private final String secretKey = "5189505EB580B685A57F3B62314CB3056B7FD40F";

    @Test
    void listBuckets(){
        try{
            ObjectStorageAuthInfo objectStorageAuthInfo = new ObjectStorageAuthInfo();
            objectStorageAuthInfo.setAccessKey(accessKey);
            objectStorageAuthInfo.setSecretKey(secretKey);

            String proxyUrl = "http://101.79.10.32:8080";

            HttpRequest httpRequest = HttpRequest.create()
                    .get()
//                    .setHost(ObjectStorageApiInfos.LIST_BUCKETS.getHost())
                    .setHost(proxyUrl)
                    .setPath(ObjectStorageApiInfos.LIST_BUCKETS.getPath());

            log.debug("#### host : {}", httpRequest.getURI().getHost());

            ListBuckets listBuckets = objectStorageHttpService.httpExecute(httpRequest, objectStorageAuthInfo, ListBuckets.class);
            log.debug("##### listBuckets : {}", listBuckets.toString());

//            Map<String, Object> responseMap
//                    = objectStorageHttpService.httpExecute(httpRequest, objectStorageAuthInfo, new TypeReference<Map<String, Object>>(){}.getType());
//            log.debug("##### responseMap : {}", responseMap.toString());

        }catch (Exception e){
            log.debug("##### Exception", e);
        }
    }

    @Test
    void listObjects(){
        try{
            ObjectStorageAuthInfo objectStorageAuthInfo = new ObjectStorageAuthInfo();
            objectStorageAuthInfo.setAccessKey(accessKey);
            objectStorageAuthInfo.setSecretKey(secretKey);

            HttpRequest httpRequest = HttpRequest.create()
                    .get()
                    .setHost(ObjectStorageApiInfos.LIST_BUCKETS.getHost())
                    .setPath("/cloit-sre-nas-backup");

            Map<String, Object> responseMap
                    = objectStorageHttpService.httpExecute(httpRequest, objectStorageAuthInfo, new TypeReference<Map<String, Object>>(){}.getType());
            log.debug("##### responseMap : {}", GsonUtils.toJson(responseMap));

        }catch (Exception e){
            log.debug("##### Exception ", e);
        }
    }

    @Test
    void sdkListBuckets(){
        final String endPoint = ObjectStorageApiInfos.LIST_BUCKETS.getHost();
        final String regionName = "kr-standard";

        try{
            final AmazonS3 s3 = AmazonS3ClientBuilder.standard()
                    .withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(endPoint, regionName))
                    .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(accessKey, secretKey)))
                    .build();
            List<Bucket> buckets = s3.listBuckets();
            log.debug("##### buckets : {}", GsonUtils.toJson(buckets));
        }catch (Exception e){
            log.debug("##### Exception ", e);
        }
    }
}
